package com.nirajan.shrestha.pubsub;

import java.net.InetAddress;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.messages.api.CoapResponse;

public class PubSubTestClient2 extends PubSubClient {	
	
	private static PubSubTestClient2 client;
	private static CoapResponse response;
	private static boolean exitAfterResponse = true;
	private static boolean reliable = true;
	
	public PubSubTestClient2() {
		super("127.0.0.1", CoapConstants.COAP_DEFAULT_PORT, new CoapClient() {
			
			@Override
			public void onResponse(CoapClientChannel channel, CoapResponse response) {
				// TODO Auto-generated method stub
				if (response.getPayload() != null && response.getObserveOption() == null) {
					/* TODO 5.2: Skip for now! In the 3rd step we replace this behavior.*/
					System.out.println("Response: " + response.toString() + " Payload: " + new String(response.getPayload()) + "; ct="+response.getContentType().getValue());
					
				} else if(response.getPayload() != null && response.getObserveOption() != null) {
					System.out.println("Response: " + response.toString() + " Observe: " + response.getObserveOption());
				}
				else {
					System.out.println("Response: " + response.toString() + "; Location: "+ new String (response.getLocationPath()));
				}
			}
			
			@Override
			public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
				// TODO Auto-generated method stub
				System.err.println("Connection Failed");
				System.exit(-1);
			}
		});
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		client = new PubSubTestClient2();
		
		System.out.println("=== START PubSubClient2 ===");	
//		client.setPath("/ps/");
//		client.create("fan", 50);
		
		/** Delete a Topic **/
//		client.setPath("/ps/window");
//		client.delete("window");
	
		/** Publish to a Topic **/
		client.setUriPath("/ps/fan");
//		client.publish("1013.1", 50); // publish without max age option
//		client.publish("1023.2", 50);
//		client.publish("1033.3", 50);
//		try {
//			Thread.sleep(2000);
//		/** publish value in every 2 seconds **/
////		client.publish("1033.3", 50);
//		}				
//		catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
//		client.publish("1033.3", 50); // publish without max age option
//		try {
//			Thread.sleep(2000);
//		/** publish value in every 2 seconds **/
////		client.publish("1033.3", 50);
//		}				
//		catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
//		client.publish("1033.3", 50); // publish without max age option
		client.publish("1033.3", 50);// publish with max age option 40		
//		for(int i = 0; i < 200; i++) {
//			client.publish("1033.3", 50, 20);
//		}
		while(true) {					
			try {
				Thread.sleep(3000);
			/** publish value in every 2 seconds **/
			client.publish("1033.3", 50);
			}				
			catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
		}
		
	}
}
