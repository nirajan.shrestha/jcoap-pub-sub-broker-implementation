package com.nirajan.shrestha.pubsub.updated;

import java.net.InetAddress;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.connection.BasicCoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapRequestCode;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.messages.api.CoapResponse;
import org.ws4d.coap.core.tools.Encoder;


public class PubSubClient implements CoapPubSubAPI,CoapClient{
	
	private boolean exitAfterResponse = true;
	/** A manager to keep track of our connections */
	private CoapChannelManager channelManager;

	/** The target where we want to send our request to */
	private CoapClientChannel clientChannel;
	
	private String path;
	private CoapRequest request;
	
	public PubSubClient(String serverAddress, int serverPort) {
		/** Get the "ChannelManager" instance **/
		this.channelManager = BasicCoapChannelManager.getInstance();

		this.clientChannel = null;

		try {
			/** Create a channel to the server **/
			this.clientChannel = this.channelManager.connect(this, InetAddress.getByName(serverAddress), serverPort);

			/** Make sure the channel is not null **/
			if (this.clientChannel == null) {
				System.err.println("Connect failed: clientChannel is null!");
				System.exit(-1);
			}
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(-1);
		}
	}	
	
	@Override
	public void setPath(String path) {
		// TODO Auto-generated method stub
		this.path = path;
	}
	

	@Override
	public void create(String topic, CoapMediaType contentType, String resourceType) {
		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.POST, this.path, true);

		/* For further exploration: other interesting options */
//		request.setPayload("<"+topic.getBytes()+">;ct="contentType.getValue()); // for PUT and POST operations you need to provide some payload
		request.setPayload(topic.getBytes());
		request.setContentType(contentType); // to define the media type of the payload
		request.setUriQuery(resourceType); // query parameters; known from e.g.: http://server.domain/resource?query=parameter
		// request.addAccept(CoapMediaType.text_plain); // only accept certain media types as response; you can add multiple
		// request.setProxyUri(proxyUri); // send this request through a proxy 
		printRequest(request);	
		this.clientChannel.sendMessage(request);
	}

	@Override
	public void create(String topic, CoapMediaType contentType, String resourceType, int maxAgeTopic) {		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.POST, this.path, true);

		/* For further exploration: other interesting options */
		request.setPayload(topic.getBytes()); // for PUT and POST operations you need to provide some payload
		request.setContentType(contentType); // to define the media type of the payload
		request.setUriQuery(resourceType); // query parameters; known from e.g.: http://server.domain/resource?query=parameter
		// request.addAccept(CoapMediaType.text_plain); // only accept certain media types as response; you can add multiple
		// request.setProxyUri(proxyUri); // send this request through a proxy 
		request.setMaxAgeTopic(maxAgeTopic);
		printRequest(request);		
		this.clientChannel.sendMessage(request);		
	
	}
	
	@Override
	public void discover() {
		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);
		
		request.setContentType(CoapMediaType.link_format);
//		request.setUriPath(path);
				
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
	}
	
	/** Resource Discovery with Resource Type (uri query) **/
	@Override
	public void discover(String resourceType) {
		// TODO Auto-generated method stub
		
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);	
		
		request.setContentType(CoapMediaType.link_format);
		
		request.setUriQuery(resourceType);
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
	}
	
	/** Resource Discovery with Content Type (uri content) **/
	@Override
	public void discover(CoapMediaType contentType) {
		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);		
		
		request.setContentType(contentType);
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
	}		
	
	
	@Override
	public void subscribe(CoapMediaType contentType) {
		// TODO Auto-generated method stub
		
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);
		
		request.setUriPath(this.path);
		
		request.setContentType(contentType);
		
		request.setToken("0x4a".getBytes());
		
		request.setObserveOption(0);
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
		
	}

	@Override
	public void publish(String payload, CoapMediaType contentType) {
		// TODO Auto-generated method stub
		
		request = this.clientChannel.createRequest(CoapRequestCode.PUT, this.path, true);
		
		request.setUriPath(this.path);
		request.setPayload(payload.getBytes());
		request.setContentType(contentType);
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
		
	}
	
	@Override
	public void publish(String payload, CoapMediaType contentType, int maxAgeTopic) {
		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.PUT, this.path, true);
		
		request.setUriPath(this.path);
		request.setPayload(payload.getBytes());
		request.setContentType(contentType);
		request.setMaxAgeTopic(maxAgeTopic);
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
	}

	@Override
	public void unsubscribe(CoapMediaType contentType) {
		// TODO Auto-generated method stub		
		request = null;

		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);
		
		request.setContentType(contentType);
		
		request.setUriPath(this.path);
		
		request.setToken("0x4b".getBytes());
		
		request.setObserveOption(1);
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
		
	}
	
	@Override
	public void read(CoapMediaType contentType) {
		// TODO Auto-generated method stub
		request = null;
		
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);
		
		request.setUriPath(path);
		
		request.setContentType(contentType);
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
		
	}
	
	@Override
	public void delete() {
		// TODO Auto-generated method stub
		request = null;
		
		request = this.clientChannel.createRequest(CoapRequestCode.DELETE, this.path, true);
		
		request.setUriPath(this.path);	
		
		printRequest(request);	
		
		this.clientChannel.sendMessage(request);
		
	}
	
	private static void printRequest(CoapRequest request){
		if(request.getPayload() != null && request.getObserveOption() == null){
			System.out.println("Request: "+request.toString() + " payload: " + Encoder.ByteToString(request.getPayload()) + " resource: " + request.getUriPath());
		}else if(request.getPayload() != null && request.getObserveOption() != null) {
			System.out.println("Request: " + request.toString() + " payload: " + request.getUriPath() + " Observe: " + request.getObserveOption() + " Token: " + request.getToken());
		}else {
			System.out.println("Request: "+request.toString() + " resource: " + request.getUriPath());
		}
	}

	@Override
	public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
		// TODO Auto-generated method stub		
		
	}
	

	@Override
	public void onResponse(CoapClientChannel channel, CoapResponse response) {
		// TODO Auto-generated method stub
		
	}
	
}
