package com.nirajan.shrestha.pubsub.updated;

import java.net.InetAddress;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.messages.api.CoapResponse;

public class PubSubTestClient3 extends PubSubClient implements CoapClient {	
	
	private static PubSubTestClient3 client;
	private static CoapResponse response;
	private static boolean exitAfterResponse = true;
	private static boolean reliable = true;
	
	public PubSubTestClient3() {
		super("127.0.0.1", CoapConstants.COAP_DEFAULT_PORT);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		client = new PubSubTestClient3();
		
		System.out.println("=== START PubSubClient3 ===");
		
		client.setPath("/ps/window");
//		
//		/** Create a Topic **/	
//		
//		client.create("handle", CoapMediaType.json, "rt=handle", 0);
//		System.out.println("Topic Created.");
		
		
		/** Create a Topic **/
//		client.Discover("/.well-known/core", true); 
//		client.Discover("/ps","rt=window", true);
		
		
//		/** Subscribe to a Topic **/
//		client.setPath("/ps/window/handle");
//		client.subscribe(CoapMediaType.json);
//		System.out.println("Subscribed to a Topic.");	
//		
//		
//		/** Sleep for 20 seconds before making an unsubscription to a topic **/
//		try {Thread.sleep(20000);
//		
//		}
//		catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
//		
//		/** UnSubscribe to a Topic **/
//		client.unsubscribe("/ps/window", reliable);
//		System.out.println("UnSubscribed to a Topic.");		
		
		
		/** Read from a Topic **/
//		client.read("/ps/window", CoapMediaType.xml, reliable);
		
		
		/** Delete a Topic **/
		client.delete();


		while(true) {
			try {Thread.sleep(5000);
				
			}
			catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
		}
		
	}
	
	@Override
	public void onResponse(CoapClientChannel channel, CoapResponse response) {
		// TODO Auto-generated method stub
//		System.out.println("In the main class");

		if (response.getPayload() != null && response.getObserveOption() == null) {
			/* TODO 5.2: Skip for now! In the 3rd step we replace this behavior.*/
			System.out.println("Response: " + response.toString() + " Payload: " + new String(response.getPayload()) + "; ct="+response.getContentType().getValue());
			
		} else if(response.getPayload() != null && response.getObserveOption() != null) {
			System.out.println("Response: " + response.toString() + " Observe: " + response.getObserveOption()+ " Payload: " + new String(response.getPayload()));
		}
		else {
			System.out.println("Response: " + response.toString());
		}
//		if (this.exitAfterResponse) {
//			System.out.println("=== STOP  PubSubClient ===");
//			//System.exit(0);
//		} 
		
	}
	
	@Override
	public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
		// TODO Auto-generated method stub
		System.err.println("Connection Failed");
		System.exit(-1);
		
	}

}
