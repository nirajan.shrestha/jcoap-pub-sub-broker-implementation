package com.nirajan.shrestha.pubsub.updated;

import java.net.InetAddress;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.messages.api.CoapResponse;

public class PubSubTestClient1 extends PubSubClient implements CoapClient {	
	
	private static PubSubTestClient1 client;
	private static CoapResponse response;
	private static boolean exitAfterResponse = true;
	private static boolean reliable = true;
	
	public PubSubTestClient1() {
		super("127.0.0.1", CoapConstants.COAP_DEFAULT_PORT);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		client = new PubSubTestClient1();
		
		System.out.println("=== START PubSubClient1 ===");
		
//		client.setPath("/ps/");
			
//		/** Create a Topic without maxAgeTopic **/ 
//		client.create("fan", CoapMediaType.json, "rt=fan");
		
//		System.out.println("Media Type: "+CoapMediaType.json.getValue()); 
		
		/** create subtopic **/
//		client.setPath("/ps/fan");
//		client.create("switch", CoapMediaType.json, "rt=fanSwich");
		
//		/** Create a Topic with maxAgeTopic **/ 
//		client.create("window", CoapMediaType.json, "rt=window", 12);
//		
//		client.setPath("/ps/");
//		client.create("door", CoapMediaType.json, "rt=window", 10);
//		System.out.println("Topic Created.");		
//		
		/** Topic Discovery **/
		client.setPath("/.well-known/core");
//		client.setPath("/.well-known/core?rt=core.ps");
		client.discover(); 
//		client.discover("rt=core.ps core.ps.discover"); 
//		client.discover("core"); 
//		client.discover("ct=50"); // ct = 50
//		client.setPath("/ps");
//		client.discover("href=/ps/fan");
//		client.discover("href=/ps/fan*"); // ct = 50
//		client.discover("rt=fan");
		
		
		/** Subscribe to a Topic **/
//		client.setPath("/ps/fan");
//		client.subscribe(CoapMediaType.json);
//		System.out.println("Subscribed to a Topic.");	
////		
//		
		/** Sleep for 20 seconds before making an unsubscription to a topic **/
		try {Thread.sleep(15000);
		
		}
		catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
		
		/** UnSubscribe to a Topic **/
//		client.setPath("/ps/fan");
//		client.unsubscribe(CoapMediaType.json);
//		System.out.println("UnSubscribed to a Topic.");		
		
		
		/** Read from a Topic **/
//		client.setPath("/ps/fan");
//		client.read(CoapMediaType.json);
//		
		
		/** Delete a Topic **/
//		client.setPath("/ps/window");
//		client.delete();


		while(true) {
			try {Thread.sleep(15000);
//			client.setPath("/ps/fan");
//			client.unsubscribe(CoapMediaType.json);
//			System.out.println("UnSubscribed to a Topic.");	
//			client.read(CoapMediaType.json);
			}
			catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
		}
		
	}
	
	@Override
	public void onResponse(CoapClientChannel channel, CoapResponse response) {
		// TODO Auto-generated method stub
//		System.out.println("In the main class");

		if (response.getPayload() != null && response.getObserveOption() == null) {
			/* TODO 5.2: Skip for now! In the 3rd step we replace this behavior.*/
			System.out.println("Response: " + response.toString() + " Payload: " + new String(response.getPayload()) + "; ct="+response.getContentType().getValue());
			
		} else if(response.getPayload() != null && response.getObserveOption() != null) {
			System.out.println("Response: " + response.toString() + " Observe: " + response.getObserveOption()+ " Payload: " + new String(response.getPayload()));
		}
		else {
			System.out.println("Response: " + response.toString() + "; Location: "+ new String (response.getLocationPath()));
		}
//		if (this.exitAfterResponse) {
//			System.out.println("=== STOP  PubSubClient ===");
//			//System.exit(0);
//		} 
//		System.out.println("Response with ALL Informations:  " + response.toString());
		
	}
	
	@Override
	public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
		// TODO Auto-generated method stub
		System.err.println("Connection Failed");
		System.exit(-1);
		
	}

}
