package com.nirajan.shrestha.pubsub.updated;

import org.ws4d.coap.core.enumerations.CoapMediaType;


public interface CoapPubSubAPI {
	
	public void setPath(String path);
	
	public void create(String payload, CoapMediaType contentType, String resourceType);
	
	public void create(String payload, CoapMediaType contentType, String resourceType, int maxAgeTopic);
	
	public void discover();
	
	public void discover(String resourceType);
	
	public void discover(CoapMediaType contentType);
	
	public void subscribe(CoapMediaType contentType);
	
	public void publish(String payload, CoapMediaType contentType);
	
	public void publish(String payload, CoapMediaType contentType, int maxAgeTopic);
	
	public void unsubscribe(CoapMediaType contentType);
	
	public void read(CoapMediaType contentType);
	
	public void delete();

}
