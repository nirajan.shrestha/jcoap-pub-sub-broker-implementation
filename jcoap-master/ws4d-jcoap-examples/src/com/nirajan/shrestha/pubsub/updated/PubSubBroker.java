package com.nirajan.shrestha.pubsub.updated;

import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.rest.CoapResourceServer;


public class PubSubBroker {

	private static PubSubBroker coapPubSubBroker;
    //private CoapResourceServer resourceServer;
    private PubSubResourceServer pubsubResourceServer;
    
    public void startServer() {
		System.out.println("=== START CoapPubSubBroker ===");
    	if (this.pubsubResourceServer != null){
    		this.pubsubResourceServer.stop();
    	}
    	
    	this.pubsubResourceServer = new PubSubResourceServer();
		
		/* this starts the server */
		try {
			/* FIXME 1.4: determine port to run */
			this.pubsubResourceServer.start(CoapConstants.COAP_DEFAULT_PORT);
		} catch (Exception e) {
		    System.err.println(e.getLocalizedMessage());
		}
		
		
		/* TODO 3.2: Skip for now! In the 2nd step we will notify about a changed resource right here */
//		while(true){
//			try {Thread.sleep(5000);}
//			catch (InterruptedException e) {/*do nothing*/}
//			for (CoapResource res : this.resourceServer.getResources().values()) {
//				res.changed();
//			}
//			resource.changed(); // Notify about changed resource
//		}
    }
	
	/* *************************************************************************************************** */
	/* Last but not least the main method to get things running                                            */
	/* *************************************************************************************************** */
	
    public static void main(String[] args) {
    	coapPubSubBroker = new PubSubBroker();
    	coapPubSubBroker.startServer();
    }
}
