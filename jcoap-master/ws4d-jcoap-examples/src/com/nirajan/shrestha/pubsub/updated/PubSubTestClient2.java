package com.nirajan.shrestha.pubsub.updated;

import java.net.InetAddress;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.messages.api.CoapResponse;

public class PubSubTestClient2 extends PubSubClient implements CoapClient {	
	
	private static PubSubTestClient2 client;
	private static CoapResponse response;
	private static boolean exitAfterResponse = true;
	private static boolean reliable = true;
	
	public PubSubTestClient2() {
		super("127.0.0.1", CoapConstants.COAP_DEFAULT_PORT);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		client = new PubSubTestClient2();
		
		System.out.println("=== START PubSubClient2 ===");	
//		client.setPath("/ps/");
//		client.create("fan", CoapMediaType.json, "rt=window");
		
		/** Delete a Topic **/
//		client.setPath("/ps/window");
//		client.delete("window");
	
		/** Publish to a Topic **/
		client.setPath("/ps/fan");
		client.publish("1033.3", CoapMediaType.json);
//		client.publish("1033.3", CoapMediaType.json, 20);
		

		while(true) {			
			
			try {Thread.sleep(5000);
			/** publish value in every 5 seconds **/
			client.publish("1033.3", CoapMediaType.json,20);
			}
			
			
			catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
		}
		
	}
	
	@Override
	public void onResponse(CoapClientChannel channel, CoapResponse response) {
		// TODO Auto-generated method stub
//		System.out.println("In the main class");

		if (response.getPayload() != null && response.getObserveOption() == null) {
			/* TODO 5.2: Skip for now! In the 3rd step we replace this behavior.*/
			System.out.println("Response: " + response.toString() + " Payload: " + new String(response.getPayload()) + "; ct="+response.getContentType().getValue());
			
		} else if(response.getPayload() != null && response.getObserveOption() != null) {
			System.out.println("Response: " + response.toString() + " Observe: " + response.getObserveOption());
		}
		else {
			System.out.println("Response: " + response.toString() + "; Location: "+ new String (response.getLocationPath()));
		}
//		if (this.exitAfterResponse) {
//			System.out.println("=== STOP  PubSubClient ===");
//			//System.exit(0);
//		} 
		
	}
	
	@Override
	public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
		// TODO Auto-generated method stub
		System.err.println("Connection Failed");
		System.exit(-1);
		
	}

}
