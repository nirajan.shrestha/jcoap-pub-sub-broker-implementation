package com.nirajan.shrestha.pubsub.updated;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.CoapServer;
import org.ws4d.coap.core.connection.BasicCoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapServerChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapRequestCode;
import org.ws4d.coap.core.enumerations.CoapResponseCode;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.messages.api.CoapResponse;
import org.ws4d.coap.core.rest.CoapData;
import org.ws4d.coap.core.rest.CoapResourceServer;
import org.ws4d.coap.core.rest.CoreResource;
import org.ws4d.coap.core.rest.api.CoapResource;

public class PubSubResourceServer extends CoapResourceServer{

	private static final Logger logger = LogManager.getLogger();
	private int port = 0;
	private Map<String, byte[]> etags = new HashMap<String, byte[]>();
	private Map<String, CoapResource> resources = new HashMap<String, CoapResource>();
	private PubSubResource pubsubResource = new PubSubResource(this);
	private CoreResource coreResource = new CoreResource(this);
	
	private Thread thread;
	private ArrayList<PubSubTopic> topicToRemove;
	
	/** toggle if the creation of resources is allowed on this server **/
	private boolean allowCreate = true;
	
	/** toggle if the creation of resources on publish is allowed on this server **/
	private boolean allowCreateOnpublish = false; // newly added, by default the broker does not allow the creation of topics on publish

	public Map<String, CoapResource> getResources() {
		return this.resources;
	}

	/**
	 * Adds a resource to the resources list and set up a resource listener.
	 * 
	 * @param resource
	 *            - The resource to add
	 */
	private void addResource(CoapResource resource) {
	  //Give the resource a callback to inform the resource server about changes.
		resource.registerServerListener(this); 
		this.resources.put(resource.getPath(), resource);
		this.pubsubResource.changed();
	}

	@Override
	public boolean createResource(CoapResource resource) {
		if (null != resource && !this.resources.containsKey(resource.getPath())) {
			addResource(resource);
			generateEtag(resource);
			logger.info("created resource: " + resource.getPath());
			this.pubsubResource.changed();
			return true;
		}
		return false;
	}

	@Override
	public boolean updateResource(CoapResource resource, CoapRequest request) {
		if (null != resource && this.resources.containsKey(resource.getPath())) {
			// sets new published data to the topic data container
			resource.put(request.getPayload(), request.getContentType()); 
			generateEtag(resource);
			logger.info("updated resource: " + resource.getPath());
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteResource(String path) {
		if (null != this.resources.remove(path)) {
			this.etags.remove(path);
			logger.info("deleted resource: " + path);
			this.pubsubResource.changed();
			return true;
		}
		return false;
	}

	@Override
	public final CoapResource getResource(String path) {
		logger.info("read resource: " + path);
		return this.resources.get(path);
	}

	@Override
	public void start() throws Exception {
		start(CoapConstants.COAP_DEFAULT_PORT);		
	}

	/**
	 * Start the ResourceServer. This usually opens network ports and makes the
	 * resources available through a certain network protocol.
	 * 
	 * @param serverport
	 *            - The port to be used.
	 * @throws Exception
	 *             if the connection can not be established
	 * @see {@link #start()} To start the server on the standard port
	 */
	public void start(int serverport) throws Exception {
		this.pubsubResource = new PubSubResource(this);
		this.coreResource = new CoreResource(this);
		this.resources.put(this.coreResource.getPath(), this.coreResource);	
		this.resources.put(this.pubsubResource.getPath(), this.pubsubResource);		
		this.port = serverport;
		BasicCoapChannelManager.getInstance().createServerListener(this, this.port);
		
/************* 	Thread for checking the maxAgeTopic lifetime *************/
		thread = new Thread(){
		    public void run(){
		    	topicToRemove = new ArrayList<PubSubTopic>();
		    	PubSubTopic topic = null;		
		    	while(true) {		    		
		    		try {
		    			for (HashMap.Entry<String, CoapResource> entry : getResources().entrySet()) {		    				
		    				if(entry.getValue() instanceof PubSubTopic) {
		    					topic = (PubSubTopic) entry.getValue();		    					
		    					System.out.println("max age set: "+topic.getmaxAgeTopicValue());
		    					System.out.println("Seconds Elapsed: "+((System.currentTimeMillis() -topic.getmaxAgeTopicValueSetTimeStamp())/1000));  					
		    					
		    					if(topic.isTopicMaxAgeLifeTimeExceeded() && !topic.isDataPublished()) {
		    						topicToRemove.add(topic); // add topics to a list whose life time expired		    						
			    					System.out.println("Topic Life Time Expired: "+entry.getKey());
			    				}
		    				}		    				
		    	        }	
		    			
		    			/** Remove Topics whose life time expired **/
						for(PubSubTopic pubsubtopic : topicToRemove) {
							if(pubsubtopic.isTopicMaxAgeLifeTimeExceeded() && !pubsubtopic.isDataPublished()) {
								getResources().remove(pubsubtopic.getPath()); // remove expired topic from the resources map
								System.out.println("Topic "+ pubsubtopic.getPath() + " removed");
							}							
						}
						topicToRemove.clear();
						
						Thread.sleep(10000);						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	}
		    	
		      }
		    };
		    thread.start();
	}

	@Override
	public void stop() {
		this.resources.clear();
		addResource(this.pubsubResource);
		this.etags.clear();
		this.pubsubResource.changed();
		// FIXME causes NullPointerException when starting the server again
		// at org.ws4d.coap.core.connection.BasicCoapChannelManager.createServerChannel(BasicCoapChannelManager.java:61)
		// BasicCoapChannelManager.getInstance().removeServerListener(this, this.port);
	}

	/**
	 * @return The port this server runs on.
	 */
	public int getPort() {
		return this.port;
	}

	@Override
	public URI getHostUri() {
		URI hostUri = null;
		try {
			hostUri = new URI("coap://" + getLocalIpAddress() + ":" + getPort());
		} catch (URISyntaxException e) {
			logger.warn("getHostUri() could not create valid URI from local IP address", e);
		}
		return hostUri;
	}

	@Override
	public void resourceChanged(CoapResource resource) {
		logger.info("Resource changed: " + resource.getPath());
	}

	@Override
	public CoapServer onAccept(CoapRequest request) {
		return this;
	}

	@Override
	public void onRequest(CoapServerChannel channel, CoapRequest request) {			
		CoapResponse response = null;
		String targetPath = null;
		String[] uriElements = null;
		List<String> uriQueries = null;
		CoapRequestCode requestCode = request.getRequestCode();
		
		try {
			if(request.getUriPath().contains("?")) {
				uriElements = request.getUriPath().split("\\?");
				uriQueries = Arrays.asList(uriElements[1]);
				targetPath = uriElements[0];
			}else {
				targetPath = request.getUriPath();
			}
		}catch(NullPointerException ex) {
//			// TODO: handle exception
			targetPath = request.getUriPath();
		}
		
		
//		try {
//			if(new String (request.getPayload()).length()!=0 && !requestCode.equals(CoapRequestCode.PUT)) {
//				targetPath = request.getUriPath()+"/"+new String (request.getPayload());
//			}else {
//				targetPath = request.getUriPath();
//			}
//			
//		}catch (NullPointerException ex) {
//			// TODO: handle exception
//			targetPath = request.getUriPath();
//		}
		
		System.out.println("Target Path: "+targetPath);
		int eTagMatch = -1;
		CoapResource resource = getResource(targetPath);

		switch (requestCode) {
		case GET:
			if (null != request.getETag()) {
				eTagMatch = request.getETag().indexOf(this.etags.get(targetPath));				
			}
			if (null == resource) {
				response = channel.createResponse(request, CoapResponseCode.Not_Found_404);				
			} else if (eTagMatch != -1) {				
				response = channel.createResponse(request, CoapResponseCode.Valid_203, CoapMediaType.text_plain);
				response.setETag(request.getETag().get(eTagMatch));
			} else if (!resource.isGetable()) {				
				response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
			} else {
				//System.out.println("CoapResourceServer Line 202"); //reached
				// Accept Formats?
				boolean matchAccept = true;
				List<CoapMediaType> mediaTypesAccepted = request.getAccept();
//				System.out.println("Media Type: "+request.getContentType());
				if (null != mediaTypesAccepted) {					
					matchAccept = false;
					for (CoapMediaType mt : mediaTypesAccepted) {
						if (resource.getAvailableMediaTypes().contains(mt)) {
							matchAccept = true;
							break;
						}
					}
					if (!matchAccept) {						
						// accepted formats option present but resource is not
						// available in the requested format
						response = channel.createResponse(request, CoapResponseCode.Not_Acceptable_406);
					}
				}
				if (matchAccept) {
					//System.out.println("CoapResourceServer Line 223");//reached					
					
					/** formats request URIs (appending rt=), if not present **/
//---------------------------------------------------------------------------------------------------------------------------					
//					Vector<String> tempuriQueries = request.getUriQuery();
					
//					System.out.println("Is temp vector empty? "+tempuriQueries.isEmpty());
//					
//					Vector<String> uriQueries = new Vector<>(); // for storing formatted request URIs (appending rt=), if not present
//					
//					if(!tempuriQueries.isEmpty()) {
//						for(String query: tempuriQueries) {
//							if(!query.startsWith("rt=") && !query.startsWith("rt=")) {
//								query = "rt="+query;
//								uriQueries.add(query);
//							}
//							else {
//								uriQueries = tempuriQueries;
//							}
//						}
//					}
//					else {
//						uriQueries = tempuriQueries;
//					}
//------------------------------------------------------------------------------------------------------------------------					
					// accepted formats option not present
					// URI queries	
					
//					Vector<String> uriQueries = request.getUriQuery();
//					Enumeration<String> sports = uriQueries.elements();
//					while (sports.hasMoreElements()) {
//					 String sport = sports.nextElement();
//					 System.out.println("uriQueries: "+sport);
//					}
//					System.out.println("in Resource the resource type: "+resource.getResourceType());
//					System.out.println("Is vector empty: "+uriQueries.isEmpty());
					
					CoapData responseValue = (null == uriQueries ? resource.get(mediaTypesAccepted)
							: resource.get(uriQueries, mediaTypesAccepted));		
					System.out.println("responseValue payload length: "+responseValue.getPayload().length);
					
					// BLOCKWISE transfer?
					if (null != request.getBlock2() || null != channel.getMaxSendBlocksize()) {
						response = channel.addBlockContext(request, responseValue.getPayload());						
					} else {
						//System.out.println("CoapResourceServer Line 234");//reached						
						
						System.out.println("Request Media Type: "+request.getContentType());
						
						System.out.println("Response Value Media Type: "+responseValue.getMediaType());
						
						if(targetPath.equals(resource.getPath())) {
							try {
								
								if(resource.getPath().equals("/.well-known/core")) {									
									//Response For discovery only
									System.out.println("inside if");
									if(responseValue.getPayload().length == 0) {
										response = channel.createResponse(request, CoapResponseCode.Not_Found_404);
									}
									else {
										System.out.println("OK1");
										response = channel.createResponse(request, CoapResponseCode.Content_205);
										System.out.println("OK2");
										response.setContentType(CoapMediaType.link_format);
										response.setPayload(responseValue);
										System.out.println("OK3");
									}
									
								}
								else if(null != request.getObserveOption()) {		
									// OBSERVE?
									if(request.getContentType().equals(responseValue.getMediaType())) {
										/** Add Subscriber to the list of observer when observe option set to 0 **/												
										if(request.getToken().length > 1  && request.getObserveOption() == 0) {
											logger.info("Request has observe option {0} and {1} token value set, "+request.getObserveOption(), new String(request.getToken()));
											
											
											response = Subscription(request, response, channel, resource, responseValue);
											

											if(resource.addObserver(request)) {
												response.setObserveOption(resource.getObserveSequenceNumber()); // setting ObserveOption 0 for response message
											}
										}
									/** Remove Subscriber from the list of observer when observe option set to 1 **/
										else if(request.getToken().length > 1  && request.getObserveOption() == 1) {
											logger.info("Request has observe option {0} and {1} token value set, "+request.getObserveOption(), new String(request.getToken()));											
											
											response = UnSubscription( request, response, channel, resource, responseValue);
											
											resource.removeObserver(request.getChannel());
											logger.info("Unsubscribed to topic {0} "+resource.getPath());
//											response.setObserveOption(resource.getObserveSequenceNumber());// setting ObserveOption 1 for response message
										}	
										else {
											logger.info("Token is not present in a Request message.");
											response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
										}
									}else {
										response = channel.createResponse(request, CoapResponseCode.Unsupported_Media_Type_415,
												responseValue.getMediaType());
									}
									
								}
								 else {
					                    //handle read operations
									response = Read(request, response, channel, resource, responseValue);
					                }
								
							}catch (Exception e) {
								// TODO: handle exception
								logger.info("Exception Occurred {0} "+e);
							}
						}
						else {
							//respond with bad request
							logger.info("Wrong Uri Path: " + request.getUriPath());
							response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
						}						
					
					}
				}
			}
			break;
		case DELETE:
			// resource not exist or can be deleted -> delete
			
				if (null != resource && resource.isDeletable()) {
					resource.delete();
					if(resource.getObserver().size()==0 ) {
						deleteResource(targetPath);
						response = channel.createResponse(request, CoapResponseCode.Deleted_202);						
					}
					/** notify subscribers if exits regarding the topic deletion **/
					else if(resource.getObserver().size()!=0 ) { 
						deleteResource(targetPath);
						response = channel.createResponse(request, CoapResponseCode.Deleted_202);
						resource.observerRemoved();
										
						ArrayList<String> resourcePaths = new ArrayList<String>();						
						
						/** remove observers of a topic (if present) after a topic is removed and send NOT FOUND message to its observers **/
						// As per https://tools.ietf.org/html/draft-ietf-core-coap-pubsub-05#page-16
								for (Map.Entry<String,CoapResource> entry : this.resources.entrySet()) {
									if(entry.getKey().contains(targetPath+"/")) {
										resourcePaths.add(entry.getKey()); // add paths to delete into an array list (resourcePaths)
										CoapResource resourceWithSubTopicObservers = entry.getValue();
										resourceWithSubTopicObservers.observerRemoved();										
									}		         				
								}
								/** delete subtopics if present **/
								for(String key: resourcePaths) {
									deleteResource(key);
								}
						/** ------------------------------------------------------------------------------------------------------ **/										
						
					}		
				}else if(null != resource && !resource.isDeletable()) {
					response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
				}else {
					response = channel.createResponse(request, CoapResponseCode.Not_Found_404); // topic does not exists
				}
			break;
		case POST:
			if(CoapMediaType.parse(request.getContentType().getValue())==(request.getContentType())) {
				if (null == resource && this.allowCreate) {
					// resource not exist & creation is allowed -> create
						try {
							if(!request.getUriQuery().isEmpty()) { // create has uri query option set
								CoapResource newResource = createResourceFromRequest(request);
								createResource(newResource); // new resource created
								logger.info(new String(request.getPayload())+" created");
								response = channel.createResponse(request, CoapResponseCode.Created_201);

//					search newly created resource uri path from the list of resources and set it as payload in a response message
								for(Map.Entry<String, CoapResource> singleResource : this.resources.entrySet()) {
									if(singleResource.getKey().equals(newResource.getPath())) {								
										response.setLocationPath(singleResource.getKey());
										break;
									}
								}
								response.setContentType(request.getContentType());	
							}
						}catch(NullPointerException ex) {
							logger.info("An attempt is made to PUBLISH using POST to a topic that does not exist");
							response = channel.createResponse(request, CoapResponseCode.Not_Found_404);
						}
									
					
				} else if (null != resource && resource.isPostable()) { //set for publish but creating second time will change the topic if the topic exists
					// resource exist & accepts post requests -> change
					logger.info(new String(request.getPayload())+" exists and its state changed");
					resource.post(request.getPayload(), request.getContentType());
					response = channel.createResponse(request, CoapResponseCode.Changed_204);
				} else if (null != resource && !resource.isPostable())  { 
//					 topic already exists
					logger.info("Topic " + new String(request.getPayload())+" already exists");
					response = channel.createResponse(request, CoapResponseCode.Forbidden_403);
				}else {
					logger.info("Bad Request");
					response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
				}
			}else{
//				Unsupported content format for topic.
				logger.info("Unsupported content format");
				response = channel.createResponse(request, CoapResponseCode.Not_Acceptable_406);
			}
			break;
		case PUT:
			if(CoapMediaType.parse(request.getContentType().getValue())==(request.getContentType())) {
				if (null != request.getIfMatchOption()) {
					eTagMatch = request.getIfMatchOption().indexOf(this.etags.get(targetPath));
				}
				if (null == resource) {
					// create
					if (null != request.getIfMatchOption()) {
						// client intended to update an existing resource
						response = channel.createResponse(request, CoapResponseCode.Precondition_Failed_412);
					} else if (!this.allowCreate) {
						// it is not allowed to create resources
						response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
					} else {
						if(this.allowCreateOnpublish) {
							// all fine, create resource (create topic on PUBLISH using PUT method)
							
							/** The Broker MUST create a topic with the URI-Path of the request, 
							 * including all of the sub-topics necessary  **/
							
							if(request.getUriPath().startsWith("/ps")) {							

								String topic = "";	
								String locationPath = "";
								for(String word: request.getUriPath().substring(4).split("/")) {
									topic = topic +"/"+word;
									request.setUriPath(topic);
									createResource(createResourceFromRequest(request));
									locationPath += " <"+this.resources.get("/ps"+topic).getPath()+">,";								
								}
								
								if(locationPath.endsWith(",")) {// remove appended , at the end
									locationPath = locationPath.substring(0, locationPath.length()-1);
								}
								response = channel.createResponse(request, CoapResponseCode.Created_201);
								response.setLocationPath(locationPath);
								
							}else {
								response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
							}
						}else {
							// Topic does not exist
							response = channel.createResponse(request, CoapResponseCode.Not_Found_404);
						}
						
//						createResource(createResourceFromRequest(request));
//						response = channel.createResponse(request, CoapResponseCode.Created_201);
					}
				} else {
					// update
					if (request.getIfNoneMatchOption() || (null != request.getIfMatchOption() && -1 == eTagMatch)) {
						// client not intent to update an existing resource
						// OR client intended to update a resource with a specific
						// eTag which is not there (anymore)
						response = channel.createResponse(request, CoapResponseCode.Precondition_Failed_412);
					} else if (!resource.isPutable()) {
						// resource did not accept put requests
						response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
					} else {
						updateResource(resource, request);
//						response to a client that published the data
						response = channel.createResponse(request, CoapResponseCode.Changed_204);											
						
						try {
							if(request.getMaxAgeTopic()>=0) {
								
								/**  If a client publishes to a Broker with the Max-Age option, the Broker
   								MUST include the same value for the Max-Age option in all notifications. **/
								((PubSubTopic) resource).setmaxAgeTopicValue(request.getMaxAgeTopic());
								
//								if(resource instanceof PubSubTopic) {
//									if(((PubSubTopic) resource).getTopicLifeTime()<0)
//									{
//										resource.setMaxAge((int)Math.abs(((PubSubTopic) resource).getTopicLifeTime()));
//									}
//								}
							}
						}catch(NullPointerException ex){
						}
				
						resource.changed(); // send notifications to the subscribers
					}
				}
			}else {
				response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
			}			
			
			break;
		default:
			response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
			break;
		}
		//System.out.println("CoapResourceServer Line 308 before sending response back to the client");//reached
		channel.sendMessage(response);

	}
	
	/** Subscribe Operation **/
	public CoapResponse Subscription(CoapRequest request, CoapResponse response, CoapServerChannel channel, CoapResource resource, CoapData responseValue) {
			
			if(resource instanceof PubSubTopic) {
				response = channel.createResponse(request, CoapResponseCode.Content_205,
						responseValue.getMediaType());
				if(((PubSubTopic) resource).isDataPublished() && ((PubSubTopic) resource).getmaxAgeTopicValue()>=0 ) {
					
					if(!((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {	
						
						response.setMaxAge((int)((PubSubTopic) resource).getTopicLifeTime());
					}
					else {
						response = channel.createResponse(request, CoapResponseCode.No_Content_207,
								responseValue.getMediaType());
					}												
				}
				else if(!((PubSubTopic) resource).isDataPublished() || ((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {
					//topic expired
					response = channel.createResponse(request, CoapResponseCode.No_Content_207,
							responseValue.getMediaType());
				}
//				else { // if needed write code to respond default value of a topic here else do nothing
//					response = channel.createResponse(request, CoapResponseCode.No_Content_207,
//							responseValue.getMediaType());
//				}
			}
			
			response.setPayload(responseValue);
			response.setContentType(responseValue.getMediaType());
			
		return response;
	}

	/** Unsubscribe Operation **/
	public CoapResponse UnSubscription(CoapRequest request, CoapResponse response, CoapServerChannel channel, CoapResource resource, CoapData responseValue) {
		logger.info("Unsubscribed to topic {0} "+resource.getPath());
		
		if(!new String(responseValue.getPayload()).equals(null)) {
//			implement to set CoapResponseCode = Content_207 for unsubscrine if topic exists but value does not exists
		}
		
		response = channel.createResponse(request, CoapResponseCode.Content_205,
				responseValue.getMediaType());
		
		response.setPayload(responseValue);
		response.setContentType(responseValue.getMediaType());
		
		return response;
	}

	
	/** Read Operation **/
	public CoapResponse Read(CoapRequest request, CoapResponse response, CoapServerChannel channel, CoapResource resource, CoapData responseValue) {
		if(request.getContentType().equals(responseValue.getMediaType())) {
			
			if(resource instanceof PubSubTopic) {
				response = channel.createResponse(request, CoapResponseCode.Content_205,
						responseValue.getMediaType());
				
				if(((PubSubTopic) resource).isDataPublished() && ((PubSubTopic) resource).getmaxAgeTopicValue()>=0 ) {
					
					if(!((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {	
						
						response.setMaxAge((int)((PubSubTopic) resource).getTopicLifeTime());
					}
					else {
						response = channel.createResponse(request, CoapResponseCode.No_Content_207,
								responseValue.getMediaType());
					}												
				}
				else if(!((PubSubTopic) resource).isDataPublished() || ((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {
					//topic expired
					response = channel.createResponse(request, CoapResponseCode.No_Content_207,
							responseValue.getMediaType());
				}
//				else { // if needed write code to respond default value of a topic here else do nothing
//					response = channel.createResponse(request, CoapResponseCode.No_Content_207,
//							responseValue.getMediaType());
//				}
			}	
			
			response.setPayload(responseValue);
			response.setContentType(responseValue.getMediaType());
			
		}
		else {
			response = channel.createResponse(request, CoapResponseCode.Unsupported_Media_Type_415,
					responseValue.getMediaType());
		}
		return response;
	}

	/**
	 * This is used to create a new resource when requested.
	 * 
	 * @param request
	 *            - the request that leads to the creation
	 * @return The resource created
	 */
	private static CoapResource createResourceFromRequest(CoapRequest request) {		
		String path = "";		
		if(request.getRequestCode().equals(CoapRequestCode.PUT)) {
			path = "/ps"+request.getUriPath();
		}
		else {
			path = request.getUriPath()+ "/" + new String(request.getPayload());
		}
				
		System.out.println("path: "+path);
		
		PubSubTopic resource = null;
		try {
			if(request.getMaxAgeTopic()>=0 && request.getRequestCode().equals(CoapRequestCode.PUT)) {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(), request.getUriPath(), 
						request.getMaxAgeTopic());	
			}else if(request.getMaxAgeTopic()>=0 && request.getRequestCode().equals(CoapRequestCode.POST)) {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(), request.getUriQuery().firstElement().substring(3), 
						request.getMaxAgeTopic());
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
			if(request.getRequestCode().equals(CoapRequestCode.PUT)) {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(), request.getUriPath());	
			}
			else {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(), request.getUriQuery().firstElement().substring(3));
			}
				
		}			
		
		return resource;
	}

	@Override
	public void onSeparateResponseFailed(CoapServerChannel channel) {
		logger.error("Separate response failed.");
	}

	@Override
	public void onReset(CoapRequest lastRequest) {
		CoapResource resource = getResource(lastRequest.getUriPath());
		if (resource != null) {
			resource.removeObserver(lastRequest.getChannel());
		}
		logger.info("Reset Message Received!");
	}

	/**
	 * @return A string containing the representation of the current IP address
	 *         or null
	 */
	private static String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (SocketException ex) {
			logger.error("Can't obtain network Interface", ex);
		}
		return null;
	}

	/**
	 * Generates an eTag for a resource and puts it to the list of eTags
	 * {@link #etags}
	 * 
	 * @param resource
	 *            - The resource that should be tagged / re-tagged
	 */
	private void generateEtag(CoapResource resource) {
		this.etags.put(resource.getPath(), ("" + resource.hashCode()).getBytes());
	}

	public boolean allowRemoteResourceCreation(boolean allow) {
		this.allowCreate = allow;
		return true;
	}

	public boolean deleteObserver(CoapResource resource) {
		// TODO Auto-generated method stub
		if(resource.deleteObserver()) {
			return true;
		}
		else {
			return false;
		}
		
		
	}
}
