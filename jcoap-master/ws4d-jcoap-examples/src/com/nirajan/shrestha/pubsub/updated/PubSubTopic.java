package com.nirajan.shrestha.pubsub.updated;

import java.util.List;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapData;
import org.ws4d.coap.core.tools.Encoder;

public class PubSubTopic extends BasicCoapResource {	
	private String result = "0.0";

	private long maxAgeTopicValue = 0;
	private long maxAgeTopicValueSetTimeStamp = 0;
	
	private boolean anyPublishes = false;
	private boolean hasMaxAgeOption = false;
	
	public PubSubTopic(String path, byte[] bs, CoapMediaType mediaType, String resourceType) {
		super(path, bs, mediaType);
				
		//disallow POST, PUT and DELETE
		this.setDeletable(true);
    	this.setPostable(true);
    	this.setPutable(true);
    	this.setObservable(true);    	
    	
    	// add some meta Information (optional)
    	this.setResourceType(resourceType);
    	this.setInterfaceDescription("GET POST PUT DELETE OBSERVE only");
    	this.setContentType(Integer.toString(mediaType.getValue()));
	}
	
	/** Create Topic with maxAgeTopic Option **/
	public PubSubTopic(String path, byte[] bs, CoapMediaType mediaType, String resourceType, int maxAgeTopic) {
		super(path, bs, mediaType);
		
		// set topic maxAgeTopic time and change time to milliseconds
		this.hasMaxAgeOption = true;
//		this.setMaxAge(maxAgeTopic);
		this.maxAgeTopicValue = (maxAgeTopic * 1000); 
		updateTopicLifeTime();		
				
		//disallow POST, PUT and DELETE
		this.setDeletable(true);
    	this.setPostable(true);
    	this.setPutable(true);
    	this.setObservable(true);    	
    	
    	// add some meta Information (optional)
    	this.setResourceType(resourceType);
    	this.setInterfaceDescription("GET POST PUT DELETE OBSERVE only");
    	this.setContentType(Integer.toString(mediaType.getValue()));
  
	}

	 @Override
	    public synchronized CoapData get(List<CoapMediaType> mediaTypesAccepted) {
	    	
	    	String result = "0.0";
	    	return new CoapData(Encoder.StringToByte(this.result), super.getCoapMediaType());
	    }
	    
	    @Override
	    public synchronized CoapData get(List<String> query, List<CoapMediaType> mediaTypesAccepted) {
	    	/* we just ignore query parameters*/
	    	return get(mediaTypesAccepted);
	    }
	    
	    @Override
		public synchronized boolean setValue(byte[] value) {
			this.result = Encoder.ByteToString(value);			
			return true;
		}
		
		@Override
		public synchronized boolean post(byte[] data, CoapMediaType type) {
			updateTopicLifeTime();
			return this.setValue(data);
		}

		@Override
		public synchronized boolean put(byte[] data, CoapMediaType type) {
			this.anyPublishes = true;
			return this.setValue(data);
		}
		public void setmaxAgeTopicValue(int maxAgeTopic) {
            this.maxAgeTopicValue = (maxAgeTopic * 1000);
            updateTopicLifeTime();	
        }
		
		public boolean hasMaxAgeOption() {
			return this.hasMaxAgeOption;
		}
		
//		public String getValue() {
//			return this.result;
//		}
		
		public boolean isDataPublished() {
			return this.anyPublishes;
		}

		public void refreshMaxAgeOption(int maxAge) {
			updateTopicLifeTime();
			this.maxAgeTopicValue = (maxAge * 1000);
		}
		
		public final synchronized void updateTopicLifeTime() {
			this.maxAgeTopicValueSetTimeStamp = System.currentTimeMillis();
        }
		
		public long getmaxAgeTopicValue() {
            return this.maxAgeTopicValue;
        }

        public long getmaxAgeTopicValueInSeconds() {
            return (this.maxAgeTopicValue / 1000);
        }
        
        public long getmaxAgeTopicValueSetTimeStamp() {
            return this.maxAgeTopicValueSetTimeStamp;
        }
		
		public synchronized long getTopicLifeTime() {
            if (this.maxAgeTopicValue == 0) {
                return 0;
            }
            return (System.currentTimeMillis() - this.maxAgeTopicValueSetTimeStamp) - this.maxAgeTopicValue;
        }

        public synchronized boolean isTopicMaxAgeLifeTimeExceeded() {
            if (this.maxAgeTopicValue == 0) {
                return false;
            }
            return (getTopicLifeTime() > 0); // if > 0, lifetime exceeded, else not exceeded
        }
		
}
