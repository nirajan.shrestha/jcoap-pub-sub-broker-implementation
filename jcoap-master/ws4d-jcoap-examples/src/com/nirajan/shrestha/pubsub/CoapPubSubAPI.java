package com.nirajan.shrestha.pubsub;

public interface CoapPubSubAPI {
	
	public void setUriPath(String path);
	
	public String getUriPath();
	
	public void create(String payload, int contentType);
	
	public void create(String payload, int contentType, int maxAgeTopic);
	
	public void discover();
	
	public void discover(String resourceType);
	
	public void subscribe(int contentType);
	
	public void publish(String payload, int contentType);
	
	public void publish(String payload, int contentType, int maxAgeTopic);
	
	public void unsubscribe(int contentType);
	
	public void read(int contentType);
	
	public void delete();

}
