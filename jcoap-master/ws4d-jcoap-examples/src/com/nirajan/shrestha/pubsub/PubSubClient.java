package com.nirajan.shrestha.pubsub;

import java.net.InetAddress;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.connection.BasicCoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapRequestCode;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.tools.Encoder;




public class PubSubClient implements CoapPubSubAPI{
	
	private boolean exitAfterResponse = true;
	/** A manager to keep track of our connections */
	private CoapChannelManager channelManager;

	/** The target where we want to send our request to */
	private CoapClientChannel clientChannel;
	
	 private ConcurrentHashMap<String, byte[]> token = new ConcurrentHashMap();
	 private final Logger logger = LogManager.getLogger();
	
	private String path;
	private CoapRequest request;
	private int contentType = 0;
	
	public PubSubClient(String serverAddress, int serverPort, CoapClient client) {
		/** Get the "ChannelManager" instance **/
		this.channelManager = BasicCoapChannelManager.getInstance();

		this.clientChannel = null;

		try {
			/** Create a channel to the server **/
			this.clientChannel = this.channelManager.connect(client, InetAddress.getByName(serverAddress), serverPort);

			/** Make sure the channel is not null **/
			if (this.clientChannel == null) {
				System.err.println("Connect failed: clientChannel is null!");
				System.exit(-1);
			}
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(-1);
		}
	}	
	
	public ConcurrentHashMap<String, byte[]> getToken(){
		return this.token;
	}
	
	@Override
	public void setUriPath(String path) {
		// TODO Auto-generated method stub
		this.path = path;
	}		

	@Override
	public String getUriPath() {
		// TODO Auto-generated method stub
		return this.path;
	}	

	@Override
	public void create(String topic, int contentType) {
		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.POST, this.path, true);
		/* For further exploration: other interesting options */
		request.setPayload(("<"+topic+">;ct="+contentType).getBytes()); // for PUT and POST operations you need to provide some payload
//		request.setPayload(("<"+topic+">;"+"<"+"switch"+">;ct="+contentType).getBytes());
		request.setContentType(CoapMediaType.link_format); // to define the media type of the payload
		printRequest(request);	
		this.clientChannel.sendMessage(request);
	}

	@Override
	public void create(String topic, int contentType, int maxAgeTopic) {		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.POST, this.path, true);
		/* For further exploration: other interesting options */
		request.setPayload(("<"+topic+">;ct="+contentType).getBytes()); // for PUT and POST operations you need to provide some payload
		request.setContentType(CoapMediaType.link_format);
		request.setMaxAgeTopic(maxAgeTopic);
		printRequest(request);		
		this.clientChannel.sendMessage(request);		
	
	}
	
	@Override
	public void discover() {
		// TODO Auto-generated method stub
		discover(null);
	}
	
	/** Resource Discovery with Resource Type (uri query) **/
	@Override
	public void discover(String query) {
		// TODO Auto-generated method stub		
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);			
		if(query!=null) {
			request.setUriQuery(query);
		}		
		request.setContentType(CoapMediaType.link_format);
		System.out.println("uri path: "+this.path);
		printRequest(request);			
		this.clientChannel.sendMessage(request);
	}
	
	/** Resource Discovery with Content Type (uri content) **/
//	@Override
//	public void discover(CoapMediaType contentType) {
//		// TODO Auto-generated method stub
//		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);		
//		
//		request.setContentType(contentType);
//		
//		printRequest(request);	
//		
//		this.clientChannel.sendMessage(request);
//	}		
//	
	
	@Override
	public void subscribe(int contentType) {
		// TODO Auto-generated method stub		
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);		
		request.setUriPath(this.path);		
		request.setContentType(CoapMediaType.parse(contentType));
		Random random = new Random();

		if(!this.token.containsKey(this.path)) {
			token.put(this.path, convertToHex(Integer.toString(random.nextInt(255-0) + 0).getBytes()).getBytes());		
			request.setToken(this.token.get(this.path));			
			request.setObserveOption(0);			
			printRequest(request);				
			this.clientChannel.sendMessage(request);
		}else {
			logger.info("Topic already subscribed");
		}				
	}

	@Override
	public void publish(String payload, int contentType) {
		// TODO Auto-generated method stub
		
		request = this.clientChannel.createRequest(CoapRequestCode.PUT, this.path, true);		
		request.setPayload(payload.getBytes());
		if(contentType > CoapMediaType.UNKNOWN.getValue()) {
			this.contentType = contentType;
		}else {
			this.contentType = 0;
		}
		request.setContentType(CoapMediaType.parse(this.contentType));		
		printRequest(request);			
		this.clientChannel.sendMessage(request);
	}
	
	@Override
	public void publish(String payload, int contentType, int maxAgeTopic) {
		// TODO Auto-generated method stub
		request = this.clientChannel.createRequest(CoapRequestCode.PUT, this.path, true);		
		request.setUriPath(this.path);
		request.setPayload(payload.getBytes());
		if(contentType > CoapMediaType.UNKNOWN.getValue()) {
			this.contentType = contentType;
		}else {
			this.contentType = 0;
		}
		request.setContentType(CoapMediaType.parse(this.contentType));
		request.setMaxAgeTopic(maxAgeTopic);		
		printRequest(request);			
		this.clientChannel.sendMessage(request);
		}

	@Override
	public void unsubscribe(int contentType) {
		// TODO Auto-generated method stub		
		request = null;
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);		
		if(contentType > CoapMediaType.UNKNOWN.getValue()) {
			this.contentType = contentType;
		}else {
			this.contentType = 0;
		}		
		if(this.token.containsKey(this.path)) {				
			request.setToken(this.token.get(this.path));			
			request.setContentType(CoapMediaType.parse(this.contentType));			
			request.setUriPath(this.path);
			request.setObserveOption(1);
//			request.setUriQuery("rt=fan");
			printRequest(request);				
			this.clientChannel.sendMessage(request);
			// have to remove the token from the concurrenthashmap if unsubscribe is successfull
		}else {
			logger.info("Token must be set to unsubscribe");
		}		
	}
	
	@Override
	public void read(int contentType) {
		// TODO Auto-generated method stub
		request = null;		
		request = this.clientChannel.createRequest(CoapRequestCode.GET, this.path, true);		
		request.setUriPath(path);
		if(contentType > CoapMediaType.UNKNOWN.getValue()) {
			this.contentType = contentType;
		}else {
			this.contentType = 0;
		}		
		request.setContentType(CoapMediaType.parse(this.contentType));		
		printRequest(request);			
		this.clientChannel.sendMessage(request);		
	}
	
	@Override
	public void delete() {
		// TODO Auto-generated method stub
		request = null;		
		request = this.clientChannel.createRequest(CoapRequestCode.DELETE, this.path, true);		
		request.setUriPath(this.path);			
		printRequest(request);			
		this.clientChannel.sendMessage(request);		
	}
	
	public static String convertToHex(byte[] bytes) {
		StringBuilder builder = new StringBuilder();
		if (bytes == null) {
			builder.append("null");
		} else {
			for(byte b : bytes) {
				builder.append(String.format("%02x", b & 0xFF));
			}
		}
		return builder.toString();
	}
	
	private static void printRequest(CoapRequest request){
		if(request.getPayload() != null && request.getObserveOption() == null){
			System.out.println("Request: "+request.toString() + " payload: " + Encoder.ByteToString(request.getPayload()) + " resource: " + request.getUriPath());
		}else if(request.getPayload() != null && request.getObserveOption() != null) {
			System.out.println("Request: " + request.toString() + " payload: " + request.getUriPath() + " Observe: " + request.getObserveOption() + " Token: " + request.getToken());
		}else {
			System.out.println("Request: "+request.toString() + " resource: " + request.getUriPath());
		}
	}
}
