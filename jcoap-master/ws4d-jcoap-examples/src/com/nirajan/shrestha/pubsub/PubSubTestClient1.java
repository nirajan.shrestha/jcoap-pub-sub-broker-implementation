package com.nirajan.shrestha.pubsub;

import java.net.InetAddress;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.messages.api.CoapResponse;

public class PubSubTestClient1 extends PubSubClient{	
	
	private static PubSubTestClient1 client;
	private static CoapResponse response;
	private static boolean exitAfterResponse = true;
	private static boolean reliable = true;
	
	public PubSubTestClient1() {
		super("127.0.0.1", CoapConstants.COAP_DEFAULT_PORT, new CoapClient() {
			
			@Override
			public void onResponse(CoapClientChannel channel, CoapResponse response) {
				// TODO Auto-generated method stub

				if (response.getPayload() != null && response.getObserveOption() == null) {
					/* TODO 5.2: Skip for now! In the 3rd step we replace this behavior.*/
					System.out.println("Response: " + response.toString() + " Payload: " + new String(response.getPayload()) + 
							"; ct="+response.getContentType().getValue());	
					if(new String(response.getToken()) !=null && client.getToken().containsKey(client.getUriPath())) {
						client.getToken().remove(client.getUriPath());
					}
					System.out.println("Path after Unsubscribe: "+client.getUriPath());
					
				} else if(response.getPayload() != null && response.getObserveOption() != null) {
					System.out.println("Response: " + response.toString() + " Observe: " + response.getObserveOption()+ 
							" Payload: " + new String(response.getPayload())+ " Token: "+new String(response.getToken()) + " Max Age: "+(int)response.getMaxAge());
					System.out.println("Path after Subscribe: "+client.getUriPath());
				}else if(new String(response.getLocationPath()) != null) {
					System.out.println("Response: " + response.toString() + "; Location: "+ new String (response.getLocationPath()));
				}else {
					System.out.println("Response: " + response.toString());
				}				
			}
			
			@Override
			public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
				// TODO Auto-generated method stub
				System.err.println("Connection Failed");
				System.exit(-1);
			}
		});
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		client = new PubSubTestClient1();		
		System.out.println("=== START PubSubClient1 ===");
		
		/** Topic Discovery **/
//		client.setUriPath("/.well-known/core?rt=core.ps");
//		client.discover(); 
		
//		client.setUriPath("/.well-known/core");
//		client.discover("rt=core.ps"); 

		/** Create a Topic without maxAgeTopic **/ 
		client.setUriPath("/ps");	
		client.create("fan", 50);
//		client.create("fans", 50);
//		client.create("fanss", 50);
//		client.setUriPath("/ps?rt=fan*");	
		try {
			Thread.sleep(3000);		
		}
		catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}

		/** create subtopic **/
//		client.setUriPath("/ps/fan");
//		client.create("switch", 50, 30);
		
		/** Create a Topic with maxAgeTopic **/ 
//		client.create("window", 50, 12);
//		System.out.println("Topic Created.");	
//		client.setUriPath("/.well-known/core");
//		client.discover("rt=*"); // success
//		client.discover("ct=50"); // success
//		client.setUriPath("/.well-known/core?ct=50"); // success
//		client.setUriPath("/ps?rt=fan*"); // success
//		client.setUriPath("/ps?rt=*");	// success
//		client.discover();
//		client.setUriPath("/ps?rt=fan"); // success
//		client.discover();
//		client.discover("core"); 
//		client.discover("ct=50"); // ct = 50 // success
//		client.setPath("/ps");
//		client.discover("href=/ps/fan");
//		client.discover("href=/ps/fan*"); // ct = 50 // success
//		client.discover("rt=fan");		
		
//		System.out.println("1st Read from a Topic.");	
//		client.setUriPath("/ps/fan");
//		client.read(40);
		
		/** Subscribe to a Topic **/
		client.setUriPath("/ps/fan");
		client.subscribe(50);
		System.out.println("Subscribed to a Topic.");	
		
		/** Sleep for 20 seconds before making an unsubscription to a topic **/
		try {
			Thread.sleep(5000);		
		}
		catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
		
		/** UnSubscribe to a Topic **/
		client.setUriPath("/ps/fan");
		client.unsubscribe(40);
		System.out.println("UnSubscribed to a Topic.");		
		
		client.setUriPath("/ps/fan/switch");
		
		
		/** Read from a Topic **/
//		System.out.println("2nd Read from a Topic.");	
//		client.setUriPath("/ps/fan");
//		client.read(40);
//		
		
		/** Delete a Topic **/
//		client.setUriPath("/ps/fan");
//		client.delete();


		while(true) {
			try {
				Thread.sleep(30000);
//				client.setUriPath("/ps/fan");
//				client.unsubscribe(40);
//				System.out.println("UnSubscribed to a Topic.");	
//			client.read(CoapMediaType.json);
			}
			catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
		}
		
	}

}
