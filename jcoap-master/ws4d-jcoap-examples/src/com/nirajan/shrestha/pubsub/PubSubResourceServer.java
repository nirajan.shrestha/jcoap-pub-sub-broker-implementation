package com.nirajan.shrestha.pubsub;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.CoapServer;
import org.ws4d.coap.core.connection.BasicCoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapServerChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapRequestCode;
import org.ws4d.coap.core.enumerations.CoapResponseCode;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.messages.api.CoapResponse;
import org.ws4d.coap.core.rest.CoapData;
import org.ws4d.coap.core.rest.CoapResourceServer;
import org.ws4d.coap.core.rest.CoreResource;
import org.ws4d.coap.core.rest.api.CoapResource;
import org.ws4d.coap.core.rest.api.ResourceServer;

public class PubSubResourceServer implements ResourceServer{

	private static final Logger logger = LogManager.getLogger();
	private int port = 0;
	private Map<String, byte[]> etags = new HashMap<String, byte[]>();
	private Map<String, CoapResource> resources = new HashMap<String, CoapResource>();
	private PubSubResource pubsubResource = new PubSubResource(this);
	private CoreResource coreResource = new CoreResource(this);
	
	private Thread thread;
	/** for flow control too many requests  **/
	private static final int request_handling_time = 100; // in seconds
	private static final int retry_time_after_too_many_requests = 8; //in seconds
	private Semaphore semaphore = new Semaphore(1);
	
	/** toggle if the creation of resources is allowed on this server **/
	private boolean allowCreate = true;
	
	/** toggle if the creation of resources on publish is allowed on this server **/
	private boolean allowCreateOnpublish = true; // newly added, by default the broker does not allow the creation of topics on publish

	public Map<String, CoapResource> getResources() {
		return this.resources;
	}

	/**
	 * Adds a resource to the resources list and set up a resource listener.
	 * 
	 * @param resource
	 *            - The resource to add
	 */
	private void addResource(CoapResource resource) {
	  //Give the resource a callback to inform the resource server about changes.
		resource.registerServerListener(this); 
		this.resources.put(resource.getPath(), resource);
		this.pubsubResource.changed();
	}

	@Override
	public boolean createResource(CoapResource resource) {
		if (null != resource && !this.resources.containsKey(resource.getPath())) {
			addResource(resource);
			generateEtag(resource);
			logger.info("created resource: " + resource.getPath());
			this.pubsubResource.changed();
			return true;
		}
		return false;
	}

	@Override
	public boolean updateResource(CoapResource resource, CoapRequest request) {
		if (null != resource && this.resources.containsKey(resource.getPath())) {
			// sets new published data to the topic data container
			resource.put(request.getPayload(), request.getContentType()); 
			generateEtag(resource);
			logger.info("updated resource: " + resource.getPath());
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteResource(String path) {
		PubSubTopic topic = (PubSubTopic) this.resources.get(path);
		if(topic.getObserver().size()!=0) { // Notify observer and remove observer of the resource if present
			topic.removeAndNotifyAll();
		}		
		if (null != this.resources.remove(path)) {
			this.etags.remove(path);
			logger.info("deleted resource: " + path);			
			return true;
		}
		return false;
	}

	@Override
	public final CoapResource getResource(String path) {
		logger.info("read resource: " + path);
		return this.resources.get(path);
	}

	@Override
	public void start() throws Exception {
		start(CoapConstants.COAP_DEFAULT_PORT);		
	}

	/**
	 * Start the ResourceServer. This usually opens network ports and makes the
	 * resources available through a certain network protocol.
	 * 
	 * @param serverport
	 *            - The port to be used.
	 * @throws Exception
	 *             if the connection can not be established
	 * @see {@link #start()} To start the server on the standard port
	 */
	public void start(int serverport) throws Exception {
		this.pubsubResource = new PubSubResource(this);
		this.coreResource = new CoreResource(this);
		this.resources.put(this.coreResource.getPath(), this.coreResource);	
		this.resources.put(this.pubsubResource.getPath(), this.pubsubResource);		
		this.port = serverport;
		BasicCoapChannelManager.getInstance().createServerListener(this, this.port);
		
/************* 	Thread for checking the maxAgeTopic lifetime *************/
		thread = new Thread(){
		    public void run(){
		    	ArrayList<PubSubTopic> topicToRemove = new ArrayList<PubSubTopic>();
		    	PubSubTopic topic = null;		
		    	while(true) {		    		
		    		try {
		    			for (HashMap.Entry<String, CoapResource> entry : getResources().entrySet()) {		    				
		    				if(entry.getValue() instanceof PubSubTopic) {
		    					topic = (PubSubTopic) entry.getValue();		    					
		    					System.out.println("max age set: "+topic.getmaxAgeTopicValue());
		    					System.out.println("Seconds Elapsed: "+((System.currentTimeMillis() -topic.getmaxAgeTopicValueSetTimeStamp())/1000));  					
		    					
		    					if(topic.isTopicMaxAgeLifeTimeExceeded() && !topic.isDataPublished()) {
		    						topicToRemove.add(topic); // add topics to a list whose life time expired		    						
			    					System.out.println("Topic Life Time Expired: "+entry.getKey());
//			    					ArrayList<String> resourcePaths = new ArrayList<String>();		
									for (Map.Entry<String,CoapResource> subTopic : getResources().entrySet()) {	
										if(subTopic.getValue() instanceof PubSubTopic) {
											if(topic != subTopic.getValue()) {
												if(subTopic.getKey().contains(topic.getPath())) {
													System.out.println(subTopic.getKey());
//													resourcePaths.add(entry.getKey()); // add paths to delete into an array list (resourcePaths)												
														topicToRemove.add((PubSubTopic) subTopic.getValue());																						
												}	
											}
										}									
											         				
									}
			    				}
		    				}		    				
		    	        }	
		    			
		    			/** Remove Topics whose life time expired **/
						for(PubSubTopic pubsubtopic : topicToRemove) {
							if(pubsubtopic.isTopicMaxAgeLifeTimeExceeded() && !pubsubtopic.isDataPublished()) {
								if(pubsubtopic.getObserver().size()!=0) { // Notify observer and remove observer of the resource if present
									pubsubtopic.removeAndNotifyAll();
								}
								getResources().remove(pubsubtopic.getPath()); // remove expired topic from the resources map
								System.out.println("Topic "+ pubsubtopic.getPath() + " removed");
							}							
						}
						topicToRemove.clear();
						
						Thread.sleep(10000);						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	}
		    	
		      }
		    };
		    thread.start();
	}

	@Override
	public void stop() {
		this.resources.clear();
		addResource(this.pubsubResource);
		this.etags.clear();
		this.pubsubResource.changed();
		// FIXME causes NullPointerException when starting the server again
		// at org.ws4d.coap.core.connection.BasicCoapChannelManager.createServerChannel(BasicCoapChannelManager.java:61)
		// BasicCoapChannelManager.getInstance().removeServerListener(this, this.port);
	}

	/**
	 * @return The port this server runs on.
	 */
	public int getPort() {
		return this.port;
	}

	@Override
	public URI getHostUri() {
		URI hostUri = null;
		try {
			hostUri = new URI("coap://" + getLocalIpAddress() + ":" + getPort());
		} catch (URISyntaxException e) {
			logger.warn("getHostUri() could not create valid URI from local IP address", e);
		}
		return hostUri;
	}

	@Override
	public void resourceChanged(CoapResource resource) {
		logger.info("Resource changed: " + resource.getPath());
	}

	@Override
	public CoapServer onAccept(CoapRequest request) {
		return this;
	}

	public boolean Matches(String string) {
		if(string == null) {
			return false;
		}
		String regex[] = {"ct=", "rt=", "rel="};
		for(String regexElement: regex) {
			try {
				Pattern p = Pattern.compile(string.substring(0, regexElement.length()));
				Matcher m = p.matcher(regexElement);
				if(m.find()) {
					return true;
				}	
			}catch (StringIndexOutOfBoundsException ex) {
				// TODO: handle exception
				return false;
			}						
		}
		return false;
	}
	public Map<String, String> deSerializePayload(String uriPath, String payload[]) {
		Map<String, String> elements = new HashMap<String, String>();
		int countCT = 0, countTopic = 0;
		for(String item: payload) {
			if(item.startsWith("<") && item.endsWith(">")) {
				countTopic++;
				if(countTopic == 1) {
					elements.put("uriPath", uriPath+"/"+item.substring(1,item.length()-1));	
					elements.put("topicOccurrance", Integer.toString(countTopic));
				}else {
					elements.put("topicOccurrance", Integer.toString(countTopic)); // ct occurred more than once in the payload
				}											
			}else if(Matches(item)) {
				countCT ++;
				if(countCT == 1) {
					elements.put("ct=", item.substring(3));
					elements.put("ctOccurrance", Integer.toString(countCT));
				}else {
					elements.put("ctOccurrance", Integer.toString(countCT)); // ct occurred more than once in the payload
				}
			}else if(Matches(item)) {
				elements.put("rt=", item.substring(3));
			}else if(Matches(item)) {
				elements.put("rel=", item.substring(4));
			}
		}
		if(!elements.containsKey("uriPath")) {
			elements.put("uriPath", null);
		}else if(!elements.containsKey("ct=")) {
			elements.put("ct=", "-1");
		}else if(!elements.containsKey("rt=")) {
			elements.put("rt=", null);
		}else if(!elements.containsKey("rel=")) {
			elements.put("rel=", null);
		}else if(!elements.containsKey("ctOccurrance")) {
			elements.put("ctOccurrance", "0");
		}else if(!elements.containsKey("topicOccurrance")) {
			elements.put("topicOccurrance", "0");
		}
		return elements;
	}
	
	 public void performBasicTopicGarbageCollection() {
		 ArrayList<PubSubTopic> topicToRemove = new ArrayList<PubSubTopic>();
		 PubSubTopic topic = null;	
		 for (HashMap.Entry<String, CoapResource> entry : getResources().entrySet()) {		    				
				if(entry.getValue() instanceof PubSubTopic) {
					topic = (PubSubTopic) entry.getValue();		    								
					
					if(topic.isTopicMaxAgeLifeTimeExceeded()) {
						topicToRemove.add(topic); // add topics to a list whose life time expired		    						
					}
				}		    				
	        }	
		 
		 for(PubSubTopic pubsubtopic : topicToRemove) {				
//					getResources().remove(pubsubtopic.getPath()); // remove expired topic from the resources map
					if(this.deleteResource(pubsubtopic.getPath())) {
						System.out.println("Topic "+ pubsubtopic.getPath() + " Max Age Time exceeded");
						System.out.println("Topic "+ pubsubtopic.getPath() + " deleted");	
					}
			}
			topicToRemove.clear();
	    }
	 
	 public void removeSubTopics(String targetPath) {
		 ArrayList<String> resourcePaths = new ArrayList<String>();		
			for (Map.Entry<String,CoapResource> entry : this.resources.entrySet()) {
				if(entry.getKey().contains(targetPath+"/")) {
					resourcePaths.add(entry.getKey()); // add paths to delete into an array list (resourcePaths)										
				}		         				
			}
			/** delete subtopics if present **/
			for(String key: resourcePaths) {
				deleteResource(key);
			}
	 }
	 
	 public CoapResponse createParentTopicsAndSubTopics(CoapServerChannel channel, CoapRequest request, String targetPath) {
		 CoapResponse response = null;
		 ArrayList<Integer> mediaTypes = new ArrayList<Integer>(){{add(40); add(50); add(60);}};
			if(mediaTypes.contains(this.resources.get(request.getUriPath()).getCoapMediaType().getValue())) {
				CoapResource newResource = createResourceFromRequest(request, targetPath);
				createResource(newResource); // new resource created
				logger.info(new String(request.getPayload())+" created");
				response = channel.createResponse(request, CoapResponseCode.Created_201);
				
//				search newly created resource uri path from the list of resources and set it as payload in a response message
				for(Map.Entry<String, CoapResource> singleResource : this.resources.entrySet()) {
					if(singleResource.getKey().equals(newResource.getPath())) {								
						response.setLocationPath(singleResource.getKey());
						break;
					}
				}
			}else {
				logger.info("Parent topic cannot have sub topics, the content format of parent must be either (link_format) or (json) or (cbor)");
				response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
			}
			return response;
	 }
	
	@Override
	public void onRequest(CoapServerChannel channel, CoapRequest request) {		
		CoapResponse response = null;
		String targetPath = null;
		CoapMediaType contentType = null;

		CoapRequestCode requestCode = request.getRequestCode();
		try {
			if(requestCode.equals(CoapRequestCode.POST)) {
				String payload = new String (request.getPayload());
					if(payload.contains(";")) {						
						Map<String, String> payloadItems = deSerializePayload(request.getUriPath(), payload.split(";"));											
						targetPath = payloadItems.get("uriPath");
						if(Integer.parseInt(payloadItems.get("topicOccurrance")) > 1) {
							logger.info("There is more than one link (topic) included in a CREATE request");
							request.setPayload((payload+"/").getBytes());
						}
						if(!(payloadItems.get("ct=").equals("-1")) && Integer.parseInt(payloadItems.get("ctOccurrance")) == 1) {
							try {
								contentType = CoapMediaType.parse(Integer.parseInt(payloadItems.get("ct=")));
							} catch (NumberFormatException e) {
								logger.info("UNKNOWN CoapMediaType or when ct value is not included or ct value is not a number in the payload");
								contentType = CoapMediaType.parse(Integer.parseInt("-1"));
							}							
						}else if(payloadItems.get("ct=").equals("-1") || Integer.parseInt(payloadItems.get("ctOccurrance")) > 1){
							logger.info("If the content format option is not included or if the option is repeated");
							contentType = CoapMediaType.parse(Integer.parseInt("-1"));					
						}
						request.setContentType(contentType);
					}else {
						targetPath = request.getUriPath(); // publish using post operation
					}				
			}else {
				if(request.getUriPath().contains("?") && !(request.getUriPath().split("\\?").length < 2)) {
					targetPath = request.getUriPath().split("\\?")[0];
					request.setUriQuery(request.getUriPath().split("\\?")[1]);
				}else {
					targetPath = request.getUriPath();
				}				
			}			
		}catch (NullPointerException ex) {
			// TODO: handle exception
			targetPath = request.getUriPath();
		}catch(IllegalStateException ex) {
			
		}
		
		System.out.println("Target Path: "+targetPath);
		
		int eTagMatch = -1;
		CoapResource resource = getResource(targetPath);

		switch (requestCode) {
		case GET:
			if (null != request.getETag()) {
				eTagMatch = request.getETag().indexOf(this.etags.get(targetPath));				
			}
			if (null == resource) {
				response = channel.createResponse(request, CoapResponseCode.Not_Found_404);				
			} else if (eTagMatch != -1) {
				/** the client included one or more ETag Options in the GET request (see Section 3.3), 
				 * notifications can have a 2.03 (Valid) response code rather than a 2.05 (Content)
				 *  response code **/
				response = channel.createResponse(request, CoapResponseCode.Valid_203, CoapMediaType.text_plain);
				response.setETag(request.getETag().get(eTagMatch));
			} else if (!resource.isGetable()) {				
				response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
			} else {
				//System.out.println("CoapResourceServer Line 202"); //reached
				// Accept Formats?
				boolean matchAccept = true;
				List<CoapMediaType> mediaTypesAccepted = request.getAccept();
//				System.out.println("Media Type: "+request.getContentType());
				if (null != mediaTypesAccepted) {					
					matchAccept = false;
					for (CoapMediaType mt : mediaTypesAccepted) {
						if (resource.getAvailableMediaTypes().contains(mt)) {
							matchAccept = true;
							break;
						}
					}
					if (!matchAccept) {						
						// accepted formats option present but resource is not
						// available in the requested format
						response = channel.createResponse(request, CoapResponseCode.Not_Acceptable_406);
					}
				}
				if (matchAccept) {					
					// accepted formats option not present
					// URI queries	
					Vector<String> uriQueries = request.getUriQuery();
					Enumeration<String> sports = uriQueries.elements();
					
					CoapData responseValue = (null == uriQueries ? resource.get(mediaTypesAccepted)
							: resource.get(uriQueries, mediaTypesAccepted));		
					
					// BLOCKWISE transfer?
					if (null != request.getBlock2() || null != channel.getMaxSendBlocksize()) {
						response = channel.addBlockContext(request, responseValue.getPayload());						
					} else {						
						request.setContentType(responseValue.getMediaType()); // only set to make it use with CoAP EXPLORER
						System.out.println("Request Media Type: "+request.getContentType());						
						System.out.println("Response Value Media Type: "+responseValue.getMediaType());

						if(targetPath.equals(resource.getPath())) {
							try {								
								if((!request.getUriQuery().isEmpty() && null == request.getObserveOption()) || request.getUriPath().equals("/.well-known/core")) {									
									//Response For discovery only									
									if(responseValue.getPayload().length == 0) {
										response = channel.createResponse(request, CoapResponseCode.Not_Found_404);
									}else {
										response = channel.createResponse(request, CoapResponseCode.Content_205,
												responseValue.getMediaType());										
										response.setPayload(responseValue);			
									}									
								}else if(null != request.getObserveOption()) {		
									// OBSERVE?
									if(request.getToken().length > 1) {
										switch(request.getObserveOption()) {
										 case 0:
											 if(request.getContentType().equals(responseValue.getMediaType())) {
												/** Add Subscriber to the list of observer when observe option set to 0 **/																						
													logger.info("Request has observe option: " + request.getObserveOption() + " and token value: " + new String(request.getToken()));																								
													response = Subscription(request, response, channel, resource, responseValue);											
								/** If a Broker is unable to accept a new Subscription on a topic, it SHOULD return the appropriate response
								 								*  code without the Observe option **/
													if(resource.addObserver(request)) {
														logger.info("New Subscription to the topic "+resource.getPath()+" accepted");
														response.setObserveOption(resource.getObserveSequenceNumber()); // setting ObserveOptionSequenceNumber for response message
													}													
												}else {
													logger.info("Unsupported Media Type "+responseValue.getMediaType()+ " on Topic: "+resource.getPath());
													response = channel.createResponse(request, CoapResponseCode.Unsupported_Media_Type_415,
															responseValue.getMediaType());
												}
											 break;
										 case 1:
											logger.info("Request has observe option: " + request.getObserveOption() + " and token value: " + new String(request.getToken()));																					
											response = UnSubscription( request, response, channel, resource, responseValue);																								
											resource.removeObserver(request.getChannel());
											logger.info("Unsubscribed to topic "+resource.getPath());													
											 break;
										 default:
											 logger.info("Request is neither subscribe or unsubscribe");
											 response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
											 break;
										}
									}else {
											logger.info("Token is not present in a Request message.");
											response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
										}																	
								}else {
					                    //handle read operations
									response = Read(request, response, channel, resource, responseValue);
					                }								
							}catch (Exception e) {
								// TODO: handle exception
								logger.info("Exception Occurred: "+e);
							}
						}else {
							//respond with bad request
							logger.info("Wrong Uri Path: " + request.getUriPath());
							response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
						}									
					}
				}
			}
			break;
		case DELETE:
			// resource not exist or can be deleted -> delete
			performBasicTopicGarbageCollection();
				if (null != resource && resource.isDeletable()) {
						if(deleteResource(targetPath)) {
							logger.info("Topic " + resource.getShortName()+" deleted");
							response = channel.createResponse(request, CoapResponseCode.Deleted_202);// response to the client which deleted the resource	
						}else {
							logger.info("Topic " + resource.getShortName()+" deletion not Successfull");
							response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
						}													
							
							/** remove observers of a subtopic (if present) after a parent topic is removed and send NOT FOUND message to its observers **/
							// As per https://tools.ietf.org/html/draft-ietf-core-coap-pubsub-05#page-16
									removeSubTopics(targetPath);
							/** ------------------------------------------------------------------------------------------------------ **/											
				}else if(null != resource && !resource.isDeletable()) {
					logger.info(" Method not allowed: " + request.getRequestCode() + " on Topic " + resource.getShortName());
					response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
				}else {
					logger.info("Topic " + resource.getShortName()+" does not exists");
					response = channel.createResponse(request, CoapResponseCode.Not_Found_404); // topic does not exists
				}
			break;
		case POST:
			performBasicTopicGarbageCollection();			
			if(!new String (request.getPayload()).contains("/")) {
				if (null == resource && this.allowCreate) {
					// resource not exist & creation is allowed -> create					
						try {							
							if(contentType.getValue() != -1 && targetPath != null) { // create has content type option set and should be a valid CoapMediaType, -1 indicates UNKNOWN
								if(this.resources.keySet().contains(request.getUriPath())) {
									response = createParentTopicsAndSubTopics(channel, request, targetPath);									
								}else {
									logger.info("Only one topic must be created per request");
									response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
								}
							}else {
								logger.info("UNKNOWN Content Media Type or Message payload Format Not Supported");
								response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
							}
						}catch(NullPointerException ex) {
							logger.info("An attempt is made to PUBLISH using POST to a topic that does not exist");
							response = channel.createResponse(request, CoapResponseCode.Not_Found_404);
						}					
					
				} else if (null != resource && resource.isPostable()) { //set for publish but creating second time will change the topic if the topic exists
					// resource exist & accepts post requests -> change
					logger.info(targetPath +" exists and its state changed");
					resource.post(request.getPayload(), request.getContentType());
					
					/** The lifetime of a topic MUST be refreshed upon create operations with a target of an existing topic.**/
					try {
						if(request.getMaxAgeTopic() >= 0) {	
							logger.info("The lifetime of a topic refreshed upon create operations with a target of an existing topic");
							((PubSubTopic) resource).refreshMaxAgeOption(request.getMaxAgeTopic());							
						}
					}catch (NullPointerException e) {
						// TODO: handle exception
					}
					response = channel.createResponse(request, CoapResponseCode.Changed_204);
				} else if (null != resource && !resource.isPostable())  { 
//					 topic already exists
					logger.info("Topic " + new String(request.getPayload())+" already exists");
					response = channel.createResponse(request, CoapResponseCode.Forbidden_403);
				}else {
					logger.info("Bad Request");
					response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
				}
			}else{
//				Unsupported content format for topic.
				logger.info("Unsupported topic format");
				response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
			}
			break;
		case PUT:
				if (null != request.getIfMatchOption()) {
					eTagMatch = request.getIfMatchOption().indexOf(this.etags.get(targetPath));
				}
				if (null == resource) {
					// create
					if (null != request.getIfMatchOption()) {
						// client intended to update an existing resource
						response = channel.createResponse(request, CoapResponseCode.Precondition_Failed_412);
					} else if (!this.allowCreate) {
						// it is not allowed to create resources
						logger.info(" Method not allowed: " + request.getRequestCode() + " on Topic " + resource.getShortName());
						response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
					} else {
						if(this.allowCreateOnpublish) {
							// all fine, create resource (create topic on PUBLISH using PUT method)
							
							/** The Broker MUST create a topic with the URI-Path of the request, 
							 * including all of the sub-topics necessary  **/
							
							if(request.getUriPath().startsWith("/ps")) {				
								String topic = "";	
								String locationPath = "";
								for(String word: request.getUriPath().substring(4).split("/")) {
									topic = topic +"/"+word;
//									request.setUriPath(topic);
									createResource(createResourceFromRequest(request, topic));
									locationPath += " <"+this.resources.get("/ps"+topic).getPath()+">,";								
								}
								
								if(locationPath.endsWith(",")) {// remove appended , at the end
									locationPath = locationPath.substring(0, locationPath.length()-1);
								}
								response = channel.createResponse(request, CoapResponseCode.Created_201);
								response.setLocationPath(locationPath);
								logger.info("Topic created on PUBLISH");								
							}else {
								response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
							}
						}else {
							// Topic does not exist
							response = channel.createResponse(request, CoapResponseCode.Not_Found_404);
							logger.info("Topic does not exist");
						}
					}
				} else {
					// update
					if(!request.getContentType().equals(resource.getCoapMediaType())) {
						logger.info("Request content format does not match with the resource content format");	
						response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
					}
					else if (request.getIfNoneMatchOption() || (null != request.getIfMatchOption() && -1 == eTagMatch)) {
						// client not intent to update an existing resource
						// OR client intended to update a resource with a specific
						// eTag which is not there (anymore)
						response = channel.createResponse(request, CoapResponseCode.Precondition_Failed_412);
					} else if (!resource.isPutable()) {
						// resource did not accept put requests
						logger.info("Resource does not accept PUT requests");	
						response = channel.createResponse(request, CoapResponseCode.Method_Not_Allowed_405);
					} else {	
						// Simple Flow Control								
							try {
								if(semaphore.tryAcquire()) {
									updateResource(resource, request);
//									response to a client that published the data
									response = channel.createResponse(request, CoapResponseCode.Changed_204);	
									logger.info("content changed");									
									try {
										if(request.getMaxAgeTopic()>=0) {
											
											/**  If a client publishes to a Broker with the Max-Age option, the Broker
											MUST include the same value for the Max-Age option in all notifications. **/
											((PubSubTopic) resource).setmaxAgeTopicValue(request.getMaxAgeTopic());
										}
									}catch(NullPointerException ex){
										/**  If a client publishes to a Broker without the Max-Age option, the Broker MUST refresh the topic lifetime 
										 * with the most recently set Max-Age value, and the Broker MUST include the most recently set Max-
											Age value in the Max-Age option of all notifications **/
										if(((PubSubTopic) resource).getmaxAgeTopicValue() > 0){
											((PubSubTopic) resource).updateTopicLifeTime();
										}
									}									
									resource.changed(); // send notifications to the subscribers
									
									// send notifications to the pendingRequest Clients if present
									if(((PubSubTopic) resource).getPendingSubscribe().size() > 0) {
										((PubSubTopic) resource).notifyPendingRequest();
										logger.info("Send notification with published data to the clients waiting for response");
									}			
									try {
										Thread.sleep(10000);		
									}
									catch (@SuppressWarnings("unused") InterruptedException e){/*do nothing*/}
									
									semaphore.release();
								}else {
									logger.info("Too many requests on topic "+resource.getPath());									
									response = channel.createResponse(request, CoapResponseCode.Too_Many_Requests_429);	
									response.setMaxAge(retry_time_after_too_many_requests);
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}finally {
								semaphore.release();
							}																
					}
				}			
			
			break;
		default:
			response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
			break;
		}
		channel.sendMessage(response);
	}
	
	/** Subscribe Operation **/
	public CoapResponse Subscription(CoapRequest request, CoapResponse response, CoapServerChannel channel, CoapResource resource, CoapData responseValue) {			
			if(resource instanceof PubSubTopic) {
				response = channel.createResponse(request, CoapResponseCode.Content_205,
						responseValue.getMediaType());
				if(((PubSubTopic) resource).isDataPublished() && ((PubSubTopic) resource).hasMaxAgeOption() ) {					
					if(!((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {	
						logger.info("Max Age option set in Response: "+((int)((PubSubTopic) resource).getTopicLifeTime())/1000 +" seconds");
						if((int)((PubSubTopic) resource).getTopicLifeTime() > 0) {
							response.setMaxAge(((int)((PubSubTopic) resource).getTopicLifeTime())/1000);
						}else {
							response.setMaxAge(Math.abs(((int)((PubSubTopic) resource).getTopicLifeTime())/1000));
						}
					}
					else {// topic is published with data and maxageOption but topic got expired.
						logger.info("Data published and has Max Option set but "+((PubSubTopic) resource).getPath() + " topic expired.");
						response = channel.createResponse(request, CoapResponseCode.Not_Found_404,
								responseValue.getMediaType()); 
					}												
				}else if(((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {
					//topic expired
					logger.info(((PubSubTopic) resource).getPath() + " topic expired.");
					response = channel.createResponse(request, CoapResponseCode.Not_Found_404,
							responseValue.getMediaType());
				}
				else if(!((PubSubTopic) resource).isDataPublished()){
//					If a Topic has been created but not yet published to when a SUBSCRIBE to the topic is received, the Broker MAY acknowledge and queue the
//					pending SUBSCRIBE and defer the response until an initial PUBLISH occurs.
					//((PubSubTopic) resource).setPendingSubscribe(request); adding a pending subscribe may not be important to add in pendingSubscribe list 
					// as the subscribers are already listed in a list of observers and will be notified once new data is published
					logger.info("Topic " + ((PubSubTopic) resource).getPath() + " is created but not yet published to.");
					response = channel.createResponse(request, CoapResponseCode.No_Content_207);
				}
				response.setPayload(responseValue);
				response.setContentType(responseValue.getMediaType());
			}else {
				logger.info("Cannot subscribe to non publish subscribe topics.");
				response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
			}			
		return response;
	}

	/** Unsubscribe Operation **/
	public CoapResponse UnSubscription(CoapRequest request, CoapResponse response, CoapServerChannel channel, CoapResource resource, CoapData responseValue) {		
		if(resource instanceof PubSubTopic) {
			response = channel.createResponse(request, CoapResponseCode.Content_205,
					responseValue.getMediaType());
			if(((PubSubTopic) resource).getmaxAgeTopicValue() >= 0 && !((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()){
				logger.info("Max Age option set in Response: "+((int)((PubSubTopic) resource).getTopicLifeTime())/1000 +" seconds");
				if((int)((PubSubTopic) resource).getTopicLifeTime() > 0) {
					response.setMaxAge(((int)((PubSubTopic) resource).getTopicLifeTime())/1000);
				}else {
					response.setMaxAge(Math.abs(((int)((PubSubTopic) resource).getTopicLifeTime())/1000));
				}				
			}
			response.setPayload(responseValue);
			response.setContentType(responseValue.getMediaType());	
		}else {
			logger.info("Cannot Unsubscribe to non publish subscribe topics.");
			response = channel.createResponse(request, CoapResponseCode.Bad_Request_400);
		}	
//		response.setPayload(responseValue);
//		response.setContentType(responseValue.getMediaType());
		return response;
	}
	
	/** Read Operation **/
	public CoapResponse Read(CoapRequest request, CoapResponse response, CoapServerChannel channel, CoapResource resource, CoapData responseValue) {
		if(request.getContentType().equals(responseValue.getMediaType())) {			
				response = channel.createResponse(request, CoapResponseCode.Content_205,
						responseValue.getMediaType());	
				if(resource instanceof PubSubTopic) {
					if(((PubSubTopic) resource).isDataPublished() && ((PubSubTopic) resource).hasMaxAgeOption()) {
						if(!((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {	
							logger.info("Max Age option set in Response: "+((int)((PubSubTopic) resource).getTopicLifeTime())/1000 +" seconds");
							if((int)((PubSubTopic) resource).getTopicLifeTime() > 0) {
								response.setMaxAge(((int)((PubSubTopic) resource).getTopicLifeTime())/1000);
							}else {
								response.setMaxAge(Math.abs(((int)((PubSubTopic) resource).getTopicLifeTime())/1000));
							}					
						}else {
							logger.info("Data published and has Max Option set but "+((PubSubTopic) resource).getPath() + " topic expired.");
							response = channel.createResponse(request, CoapResponseCode.Not_Found_404,
									responseValue.getMediaType());
						}												
					}else if(((PubSubTopic) resource).isTopicMaxAgeLifeTimeExceeded()) {
						//topic expired
						logger.info(((PubSubTopic) resource).getPath() + " topic expired.");
						response = channel.createResponse(request, CoapResponseCode.Not_Found_404,
								responseValue.getMediaType());
					}else if(!((PubSubTopic) resource).isDataPublished()){
	//					If a Topic has been created but not yet published to when a SUBSCRIBE to the topic is received, the Broker MAY acknowledge and queue the
	//					pending SUBSCRIBE and defer the response until an initial PUBLISH occurs.
						logger.info("Topic " + ((PubSubTopic) resource).getPath() + " is created but not yet published to.");
						((PubSubTopic) resource).setPendingSubscribe(request);
						response = channel.createResponse(request, CoapResponseCode.No_Content_207);
					}	
				}
				response.setPayload(responseValue);
				response.setContentType(responseValue.getMediaType());
		}
		else {
			response = channel.createResponse(request, CoapResponseCode.Unsupported_Media_Type_415);
		}
		return response;
	}

	/**
	 * This is used to create a new resource when requested.
	 * 
	 * @param request
	 *            - the request that leads to the creation
	 * @return The resource created
	 */
	private static CoapResource createResourceFromRequest(CoapRequest request, String targetPath) {		
		String path = "";
		String resourceType = "";
		if(request.getRequestCode().equals(CoapRequestCode.PUT)) {
			path = "/ps"+targetPath;
			resourceType = targetPath.substring(1);
		}else {
			path =  targetPath;
			resourceType = targetPath.substring(request.getUriPath().length()+1);
		}				
		System.out.println("path: "+path);		
		PubSubTopic resource = null;
		try {
			if(request.getMaxAgeTopic()>=0 && request.getRequestCode().equals(CoapRequestCode.PUT)) {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(), resourceType, 
						request.getMaxAgeTopic());	
			}else {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(), resourceType, 
						request.getMaxAgeTopic());
			}
		} catch (NullPointerException e) {
			// TODO: handle exception			
			if(request.getRequestCode().equals(CoapRequestCode.PUT)) {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(),resourceType);	
			}else {
				resource = new PubSubTopic(path, new String(request.getPayload()).getBytes(),
						request.getContentType(),resourceType);
			}				
		}			
		return resource;
	}

	@Override
	public void onSeparateResponseFailed(CoapServerChannel channel) {
		logger.error("Separate response failed.");
	}

	@Override
	public void onReset(CoapRequest lastRequest) {
		CoapResource resource = getResource(lastRequest.getUriPath());
		if (resource != null) {
			resource.removeObserver(lastRequest.getChannel());
		}
		logger.info("Reset Message Received!");
	}

	/**
	 * @return A string containing the representation of the current IP address
	 *         or null
	 */
	private static String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						return inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (SocketException ex) {
			logger.error("Can't obtain network Interface", ex);
		}
		return null;
	}

	/**
	 * Generates an eTag for a resource and puts it to the list of eTags
	 * {@link #etags}
	 * 
	 * @param resource
	 *            - The resource that should be tagged / re-tagged
	 */
	private void generateEtag(CoapResource resource) {
		this.etags.put(resource.getPath(), ("" + resource.hashCode()).getBytes());
	}

	public boolean allowRemoteResourceCreation(boolean allow) {
		this.allowCreate = allow;
		return true;
	}
}
