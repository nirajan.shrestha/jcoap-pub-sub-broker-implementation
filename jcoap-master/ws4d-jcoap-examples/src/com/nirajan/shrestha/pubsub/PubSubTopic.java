package com.nirajan.shrestha.pubsub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ws4d.coap.core.connection.api.CoapChannel;
import org.ws4d.coap.core.connection.api.CoapServerChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapResponseCode;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.messages.api.CoapResponse;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapData;
import org.ws4d.coap.core.tools.Encoder;

public class PubSubTopic extends BasicCoapResource {	
	private String result = "0.0";

//	retain a topic indefinitely if the Max-Age option is elided or is set to zero upon topic creation
	private long maxAgeTopicValue = 0; 
	private long maxAgeTopicValueSetTimeStamp = 0;
	
	private boolean anyPublishes = false;
	private boolean hasMaxAgeOptionOnPublish = false;
	private boolean isPendingNotificationCompleted = false;
	private int observeSequenceNumber = 0;
	
	private Map<CoapChannel, CoapRequest> pendingSubscriber = new HashMap<CoapChannel, CoapRequest>();
	
	public PubSubTopic(String path, byte[] bs, CoapMediaType mediaType, String resourceType) {
		super(path, bs, mediaType);
				
		//disallow POST, PUT and DELETE
		this.setDeletable(true);
    	this.setPostable(true);
    	this.setPutable(true);
    	this.setObservable(true);    	
    	
    	// add some meta Information (optional)
    	this.setResourceType(resourceType);
    	this.setInterfaceDescription("GET POST PUT DELETE OBSERVE only");
    	this.setContentType(Integer.toString(mediaType.getValue()));
	}
	
	/** Create Topic with maxAgeTopic Option **/
	public PubSubTopic(String path, byte[] bs, CoapMediaType mediaType, String resourceType, int maxAgeTopic) {
		super(path, bs, mediaType);
		
		// set topic maxAgeTopic time and change time to milliseconds
		this.maxAgeTopicValue = (maxAgeTopic * 1000); 
		updateTopicLifeTime();		
				
		//disallow POST, PUT and DELETE
		this.setDeletable(true);
    	this.setPostable(true);
    	this.setPutable(true);
    	this.setObservable(true);    	
    	
    	// add some meta Information (optional)
    	this.setResourceType(resourceType);
    	this.setInterfaceDescription("GET POST PUT DELETE OBSERVE only");
    	this.setContentType(Integer.toString(mediaType.getValue()));
  
	}

	 @Override
	    public synchronized CoapData get(List<CoapMediaType> mediaTypesAccepted) {
	    	
	    	String result = "0.0";
	    	return new CoapData(Encoder.StringToByte(this.result), super.getCoapMediaType());
	    }
	    
	    @Override
	    public synchronized CoapData get(List<String> query, List<CoapMediaType> mediaTypesAccepted) {
	    	/* we just ignore query parameters*/
	    	return get(mediaTypesAccepted);
	    }
	    
	    @Override
		public synchronized boolean setValue(byte[] value) {
			this.result = Encoder.ByteToString(value);			
			return true;
		}
		
		@Override
		public synchronized boolean post(byte[] data, CoapMediaType type) {
			updateTopicLifeTime();
			return this.setValue(data);
		}

		@Override
		public synchronized boolean put(byte[] data, CoapMediaType type) {
			this.anyPublishes = true;
			return this.setValue(data);
		}
		
		@Override
		public synchronized void changed() {
			if (this.getServerListener() != null) {
				this.getServerListener().resourceChanged(this);
			}
			this.observeSequenceNumber++;
			
			if (this.observeSequenceNumber > 0xFFFF) {
				this.observeSequenceNumber = 0;
			}
			this.isPendingNotificationCompleted = false;
			CoapResponse response = null;
			ArrayList<CoapChannel> observersToRemove = new ArrayList<>();
			/** notify all observers **/
			for (CoapRequest obsRequest : this.getObserver().values()) {
				
				CoapServerChannel channel = (CoapServerChannel) obsRequest.getChannel();
				
				if (this.getReliableNotification() == null) {
					response = channel.createNotification(obsRequest, CoapResponseCode.Content_205,
							this.observeSequenceNumber);
				} else {
					response = channel.createNotification(obsRequest, CoapResponseCode.Content_205,
							this.observeSequenceNumber, this.getReliableNotification());
				}
				
				CoapData data = this.get(obsRequest.getAccept());
				response.setPayload(new CoapData(data.getPayload(), data.getMediaType()));
				response.setToken(obsRequest.getToken());
				
				/** If a client publishes to a Broker with the Max-Age option, the Broker MUST
				 *  include the same value for the Max-Age option in all notifications. **/			
			
				if(this.hasMaxAgeOption()) {
					response.setMaxAge((int)this.getmaxAgeTopicValue()/1000); // in seconds
					this.updateTopicLifeTime();
				}else {
					response.setMaxAge((int)this.getmaxAgeTopicValue()/1000); // in seconds
				}

				/** Send Notification **/
				try {
					channel.sendNotification(response);					
				}catch (Exception e) {
					// TODO: handle exception
					/** If the server is unable to continue sending notifications in this format, it SHOULD send a notification with a 4.06 (Not Acceptable) 
					 * response code and subsequently MUST remove the associated entry from the list of observers of the resource. **/
					response = channel.createResponse(obsRequest, CoapResponseCode.Not_Acceptable_406);
					channel.sendNotification(response);
					observersToRemove.add(channel);
					
				}						
			}
			if(!observersToRemove.isEmpty()) {
				for(CoapChannel channel : observersToRemove) {
					this.removeObserver(channel);
				}				
			}
//			if(response.getResponseCode() == CoapResponseCode.Content_205) {
//				System.out.println("sending completed");
//				this.isPendingNotificationCompleted = true; // all subscribers are notified about the recent change of state of a resource
//			}			
		}

		/** Method to notify all observers about topic deletion **/
		public synchronized void removeAndNotifyAll() {
			for (CoapRequest obsRequest : this.getObserver().values()) {
				
				CoapServerChannel channel = (CoapServerChannel) obsRequest.getChannel();
				CoapResponse response;
				response = channel.createResponse(obsRequest, CoapResponseCode.Not_Found_404);
				response.setContentType(obsRequest.getContentType());
				response.setToken(obsRequest.getToken());
				
				channel.sendNotification(response);
			}
			this.getObserver().clear();
		}
		
		/** Method to notify the pending requests to all clients  **/
		public synchronized void notifyPendingRequest() {
			for (CoapRequest pendingRequest : this.getPendingSubscribe().values()) {
				
				CoapServerChannel channel = (CoapServerChannel) pendingRequest.getChannel();
				CoapResponse response;
				response = channel.createResponse(pendingRequest, CoapResponseCode.Content_205);		
				CoapData data = this.get(pendingRequest.getAccept());
				response.setPayload(new CoapData(data.getPayload(), data.getMediaType()));
				
				/** If a client publishes to a Broker with the Max-Age option, the Broker MUST
				 *  include the same value for the Max-Age option in all notifications. **/			
			
				if(this.hasMaxAgeOption()) {
					response.setMaxAge((int)this.getmaxAgeTopicValue()/1000); // in seconds
				}
				channel.sendNotification(response);
			}
			this.getPendingSubscribe().clear();
		}

		public void setPendingSubscribe(CoapRequest request) {
			this.pendingSubscriber.put(request.getChannel(), request);
		}
		
		public Map<CoapChannel, CoapRequest> getPendingSubscribe() {			
			return this.pendingSubscriber;
		}
		public void setmaxAgeTopicValue(int maxAgeTopic) {
            this.maxAgeTopicValue = (maxAgeTopic * 1000);
            this.hasMaxAgeOptionOnPublish = true;
            updateTopicLifeTime();	
        }
		
		public boolean hasMaxAgeOption() {
			return this.hasMaxAgeOptionOnPublish;
		}
		
		public boolean isDataPublished() {
			return this.anyPublishes;
		}

		public void refreshMaxAgeOption(int maxAge) {
			updateTopicLifeTime();
			this.maxAgeTopicValue = (maxAge * 1000);
		}
		
		public final synchronized void updateTopicLifeTime() {
			this.maxAgeTopicValueSetTimeStamp = System.currentTimeMillis();
        }
		
		public long getmaxAgeTopicValue() {
            return this.maxAgeTopicValue;
        }

        public long getmaxAgeTopicValueInSeconds() {
            return (this.maxAgeTopicValue / 1000);
        }
        
        public long getmaxAgeTopicValueSetTimeStamp() {
            return this.maxAgeTopicValueSetTimeStamp;
        }
		
		public synchronized long getTopicLifeTime() {
            if (this.maxAgeTopicValue == 0) {
                return 0;
            }
            return (this.maxAgeTopicValue - (System.currentTimeMillis() - this.maxAgeTopicValueSetTimeStamp));
        }

        public synchronized boolean isTopicMaxAgeLifeTimeExceeded() {
            if (this.maxAgeTopicValue == 0) {
                return false;
            }
            return (getTopicLifeTime() < 0); // if < 0, lifetime exceeded, else not exceeded
        }		
}
